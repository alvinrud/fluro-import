ImportContactsController.resolve = {
    // contactModel: function(FluroContent) {
    //     return FluroContent.endpoint('defined/models/contact').query().$promise;
    // }
    // pastImports: function(FluroContentRetrieval, DISTRIBUTION_KEY) {
    //     var queryDetails = {
    //         "_type": "asset"
    //     }
    //     return FluroContentRetrieval.query(queryDetails, null, null, {
    //         select: '_id data.source',
    //     }, null).then(function(res){
    //         return _.filter(res, {data:{source:DISTRIBUTION_KEY}});
    //     });
    // },
    contactModel: function(ImportContactsService) {
        return ImportContactsService.getContactModel().then(function(res){
            return res;
        })
    },

}

function ImportContactsController($scope, $log, $state, ImportContactsService, csvDataWashService, contactModel, $window) {
    var service = $scope.service = ImportContactsService;
    var contactModel = $scope.contactModel = contactModel;

    ///////////////////////////////////////////

    $scope.disableNext = true;

  	$scope.csv = {
        content: null,
        header: true,
        headerVisible: false,
        separator: ',',
        separatorVisible: false,
        result: null, // primary JSON modifiable by child states: people - families - teams - duplicates
        // encoding: 'ISO-8859-1',
        encoding: 'utf-8',
        encodingVisible: false,
        uploadButtonLabel: "upload a csv file",
        accept: '.csv',
        callback: onFileParsed,
        fluroId: 'undefined' // to be replaced with fluro importedData asset ID
    };

    ///////////////////////////////////////////

    $window.onbeforeunload = function(e) {
      var dialogText = "You are in the middle of import, if you close this tab or go back you'll lost all progress";
      e.returnValue = dialogText;
      return dialogText;
    };

    ///////////////////////////////////////////

    function onFileParsed() {
        $scope.$apply(function () {
            $scope.isReading = true;
            // console.log('parsing file')
        });

        // ngcsvimport converter csv to obj sucks, always failed to read
        var parsed = csvDataWashService.csvToJs($scope.csv.content);
        $scope.csv.result = parsed.result;
        $scope.csv.keys = parsed.headers;
        $log.debug('csv', parsed);

        $scope.columns = [];

        _.forEach($scope.csv.keys, function(key) {
            var column = service.getDefaultColumn();
            column.key = key;

            // prioritize exact match
            var found = _.find(contactModel, function(o){
              return service.isAMatch(o.title, key) || service.isAMatch(o.key, key)
            });

            // if still not found use 80% similarity rule
            if(!found) {
              found = _.find(contactModel, function(o){
                return service.isSimilar(o.key, key, 0.8) || service.isSimilar(o.title, key, 0.8);
              });
            }

            // is still not found check if contain exact word for post
            if(!found) {
                found = _.find(contactModel, function(o){
                    if(o.optgroup === 'posts') {
                        return service.hasExactString(key, o.postDefinition) || service.hasExactString(o.postDefinition, key);
                    } else {
                        return false;
                    }
                });
            }

            if(found && service.isFieldSelectable(found, $scope.columns)){
                // $log.debug('matched', key, found);
                column.fluroField = found;
                column.state = 'matched';
            }

            // if there's no data in every row skip it
            var hasData = _
                .chain($scope.csv.result)
                .map(key)
                .compact()
                .value().length;
            if(!hasData){
                column.state = 'skipped';
            }

            $scope.columns.push(column);
            $log.debug('columns', column);

        });

        $scope.$apply(function() {
            $scope.isReading = false;
            $scope.disableNext = false;

            // go to the next step
            $state.go('importContacts.match');
        });
    }




}
