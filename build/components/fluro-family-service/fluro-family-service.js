app.service('fluroFamilyService', function($log, FluroContent, FluroAccess) {

    var service = {
        update : update,
        create : create,
        getRecommendedMembers : getRecommendedMembers,
        updateInfoFromContacts : updateInfoFromContacts,
        isMatchAddresses : isMatchAddresses
    };

    return service;

    //////////////////////////////////
    //////////////////////////////////
    //////////////////////////////////
    
    /**
     * create a family
     *
     * @param      {familyObj}  example below
        {
            _id: "583ba5fee574eb140550e818",
            firstLine: "Harvey",
            title: "Specter",
            _type: "family",
            emails: [
                "harvey@pearsonspecterlitt.com"
            ],
            phoneNumbers: [
                "123123123123"
            ],
            realms: [
                "57b248cf373cb63059f584f5"
            ],
            hashtags: [ ],
            status: "active",
            address: {
                addressLine1: "601 E 54th St",
                postalCode: 10000,
                suburb: "New York City,",
                state: "New York",
                country: "United States",
                addressLine2: "Fictional Palace"
            }
        }
     * @return     {PromiseObj}  { description_of_the_return_value }
     */
    function create(family, contacts) {
        if(contacts && contacts.length) {
            family = updateInfoFromContacts(family,contacts);
        }
        return FluroContent.resource('family').save(family).$promise;
    }

    //////////////////////////////////

    function update(family, contacts) {
        if(contacts && contacts.length) {
            family = updateInfoFromContacts(family,contacts);
        }
        return FluroContent.resource('family/' + family._id).update(family).$promise;
    }

    //////////////////////////////////
    
    function updateInfoFromContacts(family, contacts) {
        $log.debug('updateInfoFromContacts BEFORE', family, contacts);
        if(typeof family === 'string') {
            return family; //if only id provided
        }

        // if(!family.emails) {
        //     family.emails = [];
        // }
        // if(!family.phoneNumbers) {
        //     family.phoneNumbers = [];
        // }
        // _.forEach(contacts, function(contact) {
        //     if(contact.emails && contact.emails.length) {
        //         family.emails = family.emails.concat(contact.emails);
        //     }
        //     if(contact.phoneNumbers && contact.phoneNumbers.length) {
        //         family.phoneNumbers = family.phoneNumbers.concat(contact.phoneNumbers);
        //     }
        // });

        if(!family.address && contacts[0].address) {
            family.address = contacts[0].address;
        }
        if(!family.title && contacts[0].lastName) {
            family.title = contacts[0].lastName;
        }
        if(!family.realms && contacts[0].realms) {
            family.realms = contacts[0].realms;
        }

        $log.debug('updateInfoFromContacts AFTER', family, contacts);

        return family;
    }

    //////////////////////////////////

    function getRecommendedMembers(keyword) {
        return FluroContent.endpoint('content/contact/search/' + keyword).query({
            limit: 10
        }).$promise;
    }

    //////////////////////////////////

    function isMatchAddresses(address1, address2) {
        if(!address1 || !address2) {
            return false;
        }

        // assume it's a match until proven wrong
        var isMatch = true;
        $log.debug('fluroFamilyService.isMatchAddresses evaluating', address1, address2);
        // country
        // if(address1.country && address2.country){
            var isSame = trimAndReplace(address1.country) == trimAndReplace(address2.country);
            if(!isSame) {
                // $log.debug('isMatchAddresses exit triggered', address1.country, address2.country);
                return false;
            }
        // } else {
        //     return false;
        // }
        // postalCode
        // if(address1.postalCode && address2.postalCode){
            var isSame = trimAndReplace(address1.postalCode) == trimAndReplace(address2.postalCode);
            if(!isSame) {
                // $log.debug('isMatchAddresses exit triggered', address1.postalCode, address2.postalCode);
                return false;
            }
        // } else {
        //     return false;
        // }
        // state
        // if(address1.state && address2.state){
            var isSame = trimAndReplace(address1.state) == trimAndReplace(address2.state);
            if(!isSame) {
                // $log.debug('isMatchAddresses exit triggered', address1.state, address2.state);
                return false;
            }
        // } else {
        //     return false;
        // }
        // suburb
        // if(address1.suburb && address2.suburb){
            var isSame = trimAndReplace(address1.suburb) == trimAndReplace(address2.suburb);
            if(!isSame) {
                return false;
            }
        // } else {
        //     return false;
        // }
        // addressLine1
        // if(address1.addressLine1 && address2.addressLine1){
            var isSame = trimAndReplace(address1.addressLine1) == trimAndReplace(address2.addressLine1);
            if(!isSame) {
                return false;
            }
        // } else {
        //     return false
        // }
        // addressLine2
        // if(address1.addressLine2 && address2.addressLine2){
            var isSame = trimAndReplace(address1.addressLine2) == trimAndReplace(address2.addressLine2);
            if(!isSame) {
                return false;
            }
        // } else {
        //     return false;
        // }
        $log.debug('isMatchAddresses end reached', isMatch, address1, address2);
        return isMatch;
    }

    function trimAndReplace(string){
        if(!string) {
            return '';
        }
        return string.toString().replace(/['" ]+/g, '').trim().toLowerCase();
    }

});