ResultsController.resolve = {
	// tags: function(FluroContent) {
	// 	return FluroContent.resource('tag').query().$promise;
	// },
	// tags: function(FluroContentRetrieval) {
 //        var queryDetails = {
 //            "_type": "tag"
 //        }
 //        return FluroContentRetrieval.query(queryDetails, null, null, {
 //            select: '_id title'
 //        }, null);
 //    }
}

function ResultsController($scope, $log, FluroContent) {

    $scope.errors = _.chain(angular.copy($scope.csv.imported))
        .forEach(function(item,key){
            item.index = key;
        })
        .filter({importState:'error'})
        .map(function(item){
            var mapped = $scope.csv.result[item.index];
            // _.forEach(item.config.data, function(val, key){
            //     if(typeof key === 'string' && typeof val === 'string') {
            //         mapped['ROW'+key] = val;
            //     }
            // })
            if(typeof item.data === 'string'){
                mapped['Error'] = item.data;
            } else {
                mapped['Error'] = _
                    .map(item.data.errors, function(val, key){
                        if(typeof key === 'string') {
                            return val.message;
                        }
                    })
                    .join(', ');
                // _.forEach(item.data.errors, function(val, key){
                //     if(typeof key === 'string') {
                //         mapped['Error'] += val.message ', ';
                //     }
                // })
            }
            return mapped;
        })
        .value();


    // get array of contacts with errors, with all matched fields accounted for, but on the results page, just display the error fields


    ////////////////////////////////////////////

	  var importedContacts = angular.copy($scope.csv.imported);
        _.forEach(importedContacts, function(contact, key) {
        contact.rowNo = key + 2; //get row number
    });

    console.log('Imported',$scope.csv.imported)

	  $scope.getErrors = _.chain(importedContacts) //take all the imported contacts
        .filter({importState:'error'}) //filter out just the ones with errors

        // .map(function(contact){
        //     var mapped = {
        //         rowNo: contact.rowNo
        //     };
        //     _.forEach(contact.config.data, function(val, key){
        //         if(typeof key === 'string' && typeof val === 'string') {
        //             mapped['row.'+key] = val;
        //         }
        //     })
        //     _.forEach(contact.data.errors, function(val, key){
        //         if(typeof key === 'string') {
        //             mapped['err.'+key] = val.message;
        //         }
        //     })
        //     return mapped;
        // })
        .value();


    console.log('Contacts with Errors', $scope.getErrors)

    ////////////////////////////////////////////


    if($scope.errors.length) {
      FluroContent.resource('importedData/' + $scope.csv.fluroId).update({
        data:{
          errors: $scope.errors
        }
      })
    }

    
    $scope.headers = function(errors) {
      var keys = Object.keys(errors[0]);
      _.pull(keys, '$$hashKey')
      return keys;
    }
}




