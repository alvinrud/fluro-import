/**
* @desc fluro contact directive that display action options based on current selection
* !important note: always use subobject as ng-model (ie. config.realms) so this directive has access to modify
* @example <accordion-realm-selector ng-model="item.realms" pre-selected="fluroObj.realms" action="'create contact'"></accordion-realm-selector>
*/
app.directive('accordionRealmSelector', function(){
	// Runs during compile
	return {
		// name: '',
		// priority: 1,
		// terminal: true,
		scope: {
			model: '=ngModel', // array of realms
			action: '=', // optional array of realms to select
			preSelected: '=?' // optional array of realms pre-selected
		}, // {} = isolate, true = child, false/undefined = no change
		controller: 'AccordionRealmSelectorController',
		require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
		restrict: 'E', // E = Element, A = Attribute, C = Class, M = Comment
		// template: '',
		templateUrl: 'accordion-realm-selector/accordion-realm-selector.html',
		replace: true,
		transclude: true,
		// compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
		link: function($scope, iElm, iAttrs, controller) {
		}
	};
});

app.controller('AccordionRealmSelectorController', function($scope, FluroAccess, FluroContent, FluroContentRetrieval){
	
	$scope.model = (!$scope.model) ? [] : $scope.model;

	$scope.$watch('realms', function(realms){
		if(!realms) {
			return;
		}
		$scope.model = getSelected();
	},true);

	//////////////////////////////////
	
	// assign default realms options to select
	$scope.realms = FluroAccess.retrieveActionableRealms($scope.action);
	applyPreSelection();

	if(!$scope.realms.length) {
		FluroContent.resource('realm').query({
			fields: ['_id', 'title', 'color', 'bgColor', 'created']
		}).$promise.then(function(res){
			$scope.realms = res;
			applyPreSelection();
		})
	} else if(typeof $scope.realms[0] === 'string') {
		console.log('realms', $scope.realms);
		FluroContentRetrieval.retrieveMultiple($scope.realms)
			.then(function(res){
				$scope.realms = res;
				applyPreSelection();
			})
	}

	//////////////////////////////////

	function applyPreSelection() {
		if(!$scope.preSelected){
			return;
		}
		_.forEach($scope.realms, function(realm){
			if(_.find($scope.preSelected, {_id:realm._id})) {
				realm.selected = true;
			}
		})
	}

	//////////////////////////////////
	
	function getSelected() {
		return _.filter($scope.realms, {selected: true});
	}

});