var app = angular.module('fluro', [
    'ngAnimate',
    'ngResource',
    'ui.router',
    'ngTouch',
    'fluro.config',
    'fluro.access',
    'fluro.validate',
    'fluro.interactions',
    'fluro.content',
    'fluro.asset',
    'fluro.socket',
    'fluro.video',
    'angular.filter',
    'formly',
    'formlyBootstrap',
    'ui.bootstrap',
    'angularFileUpload',
    'ngCsvImport',
    'yaru22.jsonHuman',
    'ngSanitize',
    'ngCsv'
])



/////////////////////////////////////////////////////////////////////

function getMetaKey(stringKey) {
    var metas = document.getElementsByTagName('meta');

    for (i = 0; i < metas.length; i++) {
        if (metas[i].getAttribute("property") == stringKey) {
            return metas[i].getAttribute("content");
        }
    }
    return "";
}

/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

app.config(function($stateProvider, $logProvider, $httpProvider, FluroProvider, $urlRouterProvider, $locationProvider) {

    $logProvider.debugEnabled(false);
    // $logProvider.debugEnabled(true);

    ///////////////////////////////////////////////////

    var access_token = getMetaKey('fluro_application_key');

    //API URL
    var api_url = getMetaKey('fluro_url');

    FluroProvider.set({
        apiURL: api_url,
        token: access_token,
        sessionStorage: true,
    });

    ///////////////////////////////////////////

    //Http Intercpetor to check auth failures for xhr requests
    if (!access_token) {
        $httpProvider.defaults.withCredentials = true;
    }

    $httpProvider.interceptors.push('FluroAuthentication');

    ///////////////////////////////////////////

    $locationProvider.html5Mode(true);

    ///////////////////////////////////////////
    ///////////////////////////////////////////
    ///////////////////////////////////////////

    // $stateProvider.state('home', {
    //     url: '/',
    //     templateUrl: 'routes/home/view.html',
    //     // controller: '',
    //     resolve: {
    //         // session: function($stateParams, FluroContent) {
    //         //     return FluroContent.endpoint('session').get().$promise;
    //         // },
    //     }
    // });

     // $stateProvider.state('importContacts', {
    //     url: '/contacts',
    //     templateUrl: 'routes/importContacts/select.html',
    //     controller: ImportContactsController,
    //     resolve: ImportContactsController.resolve
    // });

    //////////////////////////////////////////

    $stateProvider.state('importContacts', {
        url: '',
        abstract:true,
        template:'<div ui-view class="import-view"></div>',
        controller: ImportContactsController,
        resolve: ImportContactsController.resolve
    });

    ///////////////////////////////////////////

     $stateProvider.state('importContacts.select', {
        url: '/',
        templateUrl: 'routes/select/select.html',
    });

    ///////////////////////////////////////////

    $stateProvider.state('importContacts.match', {
        url: '/match',
        templateUrl: 'routes/match/match.html',
        controller: MatchController,
        resolve: MatchController.resolve
    });

    ///////////////////////////////////////////

    $stateProvider.state('importContacts.confirm', {
        url: '/confirm',
        templateUrl: 'routes/confirm/confirm.html',
        controller: ConfirmationController,
        resolve: ConfirmationController.resolve
    });

    ///////////////////////////////////////////

    $stateProvider.state('importContacts.result', {
        url: '/result',
        templateUrl: 'routes/results/result.html',
        controller: ResultsController,
        resolve: ResultsController.resolve
    });

    ///////////////////////////////////////////

    $stateProvider.state('imported', {
        url: '/imported',
        templateUrl: 'routes/imported/list.html',
        controller: ImportedListController,
        resolve: ImportedListController.resolve
    });

    ///////////////////////////////////////////

    $stateProvider.state('importedView', {
        url: '/imported/:importId',
        templateUrl: 'routes/imported/view.html',
        controller: ImportedViewController,
        resolve: ImportedViewController.resolve
    });

    ///////////////////////////////////////////

    $urlRouterProvider.otherwise("/");


});

/////////////////////////////////////////////////////////////////////

app.run(function($rootScope, $sessionStorage, Asset, FluroContent, FluroBreadcrumbService, FluroScrollService, $location, $timeout, $state, $log) {

    //Global variable for if we are testing live
    //or locally
    $rootScope.staging = true;


    $rootScope.asset = Asset;
    $rootScope.$state = $state;
    $rootScope.session = $sessionStorage;
    $rootScope.breadcrumb = FluroBreadcrumbService;

    //////////////////////////////////////////////////////////////////

    //Get the session of the current user
    FluroContent.endpoint('session')
    .get()
    .$promise.then(function(res) {
        $rootScope.user = res;
    }, function(err) {
        console.log('ERROR LOADING USER');
        $rootScope.user = null;
    });

    //////////////////////////////////////////////////////////////////

    $rootScope.logout = function() {
        //Sign out
        $rootScope.user = null;
    }

    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////

    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, error) {
        //Close the sidebar
        $rootScope.sidebarExpanded = false;
    });


    $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
        throw error;
    });

    //////////////////////////////////////////////////////////////////

    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams, error) {
        $rootScope.currentState = toState.name;

        // if back is pressed after successful import redirect to select csv
        if(fromState.name === 'importContacts.result') {
         $log.debug('$stateChangeSuccess redirect back to importContacts.select');
         $state.go('importContacts.select');
        }
    });

    //////////////////////////////////////////////////////////////////


    $rootScope.getTypeOrDefinition = function(item, defaultIfNoneProvided) {
        if (item.definition && item.definition.length) {
            return item.definition;
        }

        if (!item._type && defaultIfNoneProvided) {
            return defaultIfNoneProvided;
        }


        return item._type;
    }



    //////////////////////////////////////////////////////

    //Make touch devices more responsive
    FastClick.attach(document.body);

});
