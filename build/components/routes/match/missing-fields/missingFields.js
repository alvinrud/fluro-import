app.component('missingFields', {
  templateUrl: 'routes/match/missing-fields/missingFields.html',
  bindings: {
    resolve: '=',
    close: '&',
    dismiss: '&'
  },
  controller: function (ImportContactsService, $state) {
    var $ctrl = this;
    $ctrl.service = ImportContactsService;



    $ctrl.$onInit = function () {
      $ctrl.columns = $ctrl.resolve.columns;
      $ctrl.requiredFields = $ctrl.resolve.requiredFields;
      setFilteredColumn();
    };


    function setFilteredColumn(){
      $ctrl.filteredColumns = _.filter($ctrl.resolve.columns, function(col) {
        return !col.fluroField;
      });
    }


    $ctrl.apply = function(field) {
      var col = _.find($ctrl.resolve.columns, {key: field.colSelectionKey});
      col.fluroField = field;

      field.matchedTo = String(field.colSelectionKey); //Keep note of what column is matched to the required Fluro field while on the Modal
      // field.columns = [col];
      field.isMissing = false;

      setFilteredColumn(); // making sure what's been applied taken out of the list
      $ctrl.resolve.saveCol(col);
    }


    $ctrl.ok = function () {
      $state.go('importContacts.confirm');
      $ctrl.close({$value: 'ok'});
    };

    $ctrl.cancel = function () {
      $ctrl.dismiss({$value: 'cancel'});
    };

  }
});