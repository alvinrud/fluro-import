app.service('ImportContactsService', function(FluroContent, contactModelService, fluroContactService, $log, $q, DISTRIBUTION_KEY) {

	// var config = {
	// 	// optOutEmail: false,
	// 	// optOutSMS: false,
	// 	// realms: [], // accordion-realm-selector
	// 	// availableRealms: realms,
	// 	// availableTags: tags,
	// 	// importId: '',
	// };

	var service = {
		config: {}, //!! required for import,

		// data model
		contactModel: null, // stored for getContactModel
		getDefaultColumn: getDefaultColumn,
		getContactModel: getContactModel,
		reloadContactModel: reloadContactModel,

		// comparing string/keys
		isAMatch: isAMatch,
		isSimilar: isSimilar,
		trimAndReplace: trimAndReplace,
		hasExactString: hasExactString,

		// fields mapping utils
		getRequiredFields: getRequiredFields,
		isFieldMapped: isFieldMapped,
		isFieldSelectable: isFieldSelectable,
		isRequiredFieldsMapped: isRequiredFieldsMapped,

		// primary
		getMappedRow: getMappedRow,
		startImport: startImport,
	};

	return service;

	///////////////////////////////////////
	///////////////////////////////////////
	// data model
	///////////////////////////////////////

	function getDefaultColumn(){
		var defaultColumn = {
			key: '', // csv column header
			fluroField: '', // a fluro field to map to, see getContactModel
			state: 'unmatched' // unmatched - edit - skipped - matched
		}
		return angular.copy(defaultColumn);
	}

	function getContactModel(){
		$log.debug('getContactModel');
        if (service.contactModel) {
            var deferred = $q.defer();
            deferred.resolve(service.user);
            return deferred.promise;
        } else {
		    return service.reloadContactModel();
        }
	}

	function reloadContactModel(){
		$log.debug('reloadContactModel');
		return contactModelService
			.then(function(res){
				service.contactModel = res;
				return res;
			});
	}

	///////////////////////////////////////
	///////////////////////////////////////
	// comparing string/keys
	///////////////////////////////////////

	/**
	 * replace all non (alphanumeric and underscore) on a string for comparison purpose, ie. 'tEst\nT e" st_7' => 'testtest_7'
	 *
	 * @param      {string}  string  The string
	 * @return     {string}  { trimmed_and_replaced_lowercase }
	 */
	function trimAndReplace(string){
		return string.replace(/[^a-zA-Z0-9_]+/g, '').trim().toLowerCase();
	}

	function isAMatch(key1, key2) {
		key1 = trimAndReplace(key1);
		key2 = trimAndReplace(key2);
		return key1 == key2;
	}

	function isSimilar(key1, key2, similarityValue) {
		if(!similarityValue) {
			similarityValue = 0.8;
		}
		key1 = trimAndReplace(key1);
		key2 = trimAndReplace(key2);
		var isSimilar = similarity(key1,key2) >= similarityValue;
		return isSimilar;
	}

	function hasExactString(key1, key2) {
		key1 = trimAndReplace(key1);
		key2 = trimAndReplace(key2);
		var index = key1.search(new RegExp(key2));
		return index >= 0;
	}

	///////////////////////////////////////
	// private

	//http://stackoverflow.com/questions/10473745/compare-strings-javascript-return-of-likely
	function similarity(s1, s2) {
	  var longer = s1;
	  var shorter = s2;
	  if (s1.length < s2.length) {
	    longer = s2;
	    shorter = s1;
	  }
	  var longerLength = longer.length;
	  if (longerLength == 0) {
	    return 1.0;
	  }
	  return (longerLength - editDistance(longer, shorter)) / parseFloat(longerLength);
	}

	//http://stackoverflow.com/questions/10473745/compare-strings-javascript-return-of-likely
	function editDistance(s1, s2) {
	  s1 = s1.toLowerCase();
	  s2 = s2.toLowerCase();

	  var costs = new Array();
	  for (var i = 0; i <= s1.length; i++) {
	    var lastValue = i;
	    for (var j = 0; j <= s2.length; j++) {
	      if (i == 0)
	        costs[j] = j;
	      else {
	        if (j > 0) {
	          var newValue = costs[j - 1];
	          if (s1.charAt(i - 1) != s2.charAt(j - 1))
	            newValue = Math.min(Math.min(newValue, lastValue),
	              costs[j]) + 1;
	          costs[j - 1] = lastValue;
	          lastValue = newValue;
	        }
	      }
	    }
	    if (i > 0)
	      costs[s2.length] = lastValue;
	  }
	  return costs[s2.length];
	}


	///////////////////////////////////////
	///////////////////////////////////////
	// fields mapping utils
	///////////////////////////////////////

	function getRequiredFields(columns) {
		var requiredFields = [];
		var fieldKeys = [
			'firstName',
			'lastName',
			'emails',
			'phoneNumbers'
		];

		_.forEach(fieldKeys, function(key) {
			var field = _.find(angular.copy(service.contactModel), {key: key});

			field.isMissing = !isFieldMapped(key, columns);
			if(!field.isMissing) {
				field.columns = _.filter(angular.copy(columns), {fluroField: {key:key}, state: 'matched'});
			}
			requiredFields.push(field);
		});

		return requiredFields;
	}

	function isRequiredFieldsMapped(columns) {
		$log.debug('isRequiredFieldsMapped',
			isFieldMapped('firstName', columns),
			isFieldMapped('lastName', columns),
			isFieldMapped('emails', columns),
			isFieldMapped('phoneNumbers', columns)
		);

		// required emails or phoneNumbers
		var hasEmailOrPhone = isFieldMapped('emails', columns) || isFieldMapped('phoneNumbers', columns);

		return isFieldMapped('firstName', columns) && isFieldMapped('lastName', columns) && hasEmailOrPhone;
	}

	function isFieldMapped(fieldKey, columns) {
		return _.some(columns, {fluroField: {key:fieldKey}, state: 'matched'});
	}


	function isFieldSelectable(field, columns) {
		var isSelectable;

		switch (field.optgroup) {
			case 'teams':
				// team can always be selectable multiple times
				isSelectable = true;
				break;
			case 'posts':
				if(isFieldMapped(field.key, columns)){
					// if been map check for postDefinition
					var mappedField = _.find(columns, {fluroField: {key:field.key}, state: 'matched'});
					isSelectable = (mappedField.fluroField.postDefinition !== field.postDefinition);
				} else {
					isSelectable = true;
				}
				break;
			default:
				switch (field.key) {
					case 'emails':
					case 'phoneNumbers':
					case 'tags':
					case 'realms':
						// can be selected multiple times
						isSelectable = true;
						break;
					default:
						// otherwise selectable if hasn't been mapped
						isSelectable = !isFieldMapped(field.key, columns);
						break;
				}
				break;
		}

		return isSelectable;
	}


	/**
     * generate contact object based on csv row and header/columns, creating tags and realms in the process
     *
     * @param      {row}  (key,value) JS object from a converted csv row
     * @param      {columns}  csv header to fluro mapping configuration
     * @param      {extraColumn}  extra column to add as contact.data
     * @return     {mappedRow promise} key value pair of contact, family, posts
     */

	function getMappedRow(row, columns, extraColumns) {
		// var splitRegex = /,\s*/;
		var splitRegex = /[,;\|]\s*/; // split string by , ; or |
		var promises = []; // array of promises for creating new realms and tags

		var contactObj = {
				data: {
					channel: DISTRIBUTION_KEY,
					importId: service.config.importId,
				}
			}; // primary object to return
		var familyObj = {
			data: {
				channel: DISTRIBUTION_KEY,
				importId: service.config.importId
			}
		}; // temporary default family object to add to contactObj, will be replaced if matching found

		var teams = []; //expect [{teamObj}]

		var posts = {}; //expect {
		                //          comment: {data:{}},
		                //          note: {data:{}},
		                //       }


		_.each(extraColumns, function(extraColumn) {
			if(row[extraColumn.key] && row[extraColumn.key].length) {
			var escapedSafeKey = extraColumn.key.replace(/\./g,'_');
			contactObj.data[escapedSafeKey] = row[extraColumn.key];
			}
		});


		_.forEach(columns, function(col) {

			switch(col.fluroField.optgroup) {
				case 'contact':
					switch(col.fluroField.key) {
						case 'lastName':
							contactObj[col.fluroField.key] = row[col.key];
							familyObj.title = row[col.key];
							break;
				    case 'emails':
				    case 'phoneNumbers':
				    	// make sure it starts with an array
				    	if(!angular.isDefined(contactObj[col.fluroField.key])) {
				    		contactObj[col.fluroField.key] = [];
				    	}
				    	// start adding row values
				    	if(row[col.key] && row[col.key].length) {
				    		contactObj[col.fluroField.key].push(row[col.key]);
				    	}
				        break;
				    case 'created':
				    	if(typeof row[col.key] == 'string'){
				    		var match = row[col.key].match(new RegExp(/\d{4}\-\d{2}-\d{2}/g));
				    		if(match){
				    			contactObj[col.fluroField.key] = new Date(match[0]);
				    		}
				    	}
				    	break;
				    case 'dob':
				    	if(typeof row[col.key] == 'string' && row[col.key].length){
				    		var match = row[col.key].match(new RegExp(/\d{4}\-\d{2}-\d{2}/g));
				    		$log.debug('dob, row, col', match, row, col);
				    		if(match){
				    			contactObj[col.fluroField.key] = new Date(match[0]);
				    			contactObj.dobVerified = true;
				    		} else {

				    			var attemptDate =Date.parse(row[col.key]);

				    			if(!isNaN(attemptDate)) {
				    				contactObj[col.fluroField.key] = attemptDate;
				    			}
				    		}
				    	}
				    	break;
				    case 'realms':
				    	// make sure it starts with an array
				    	if(!angular.isDefined(contactObj[col.fluroField.key])) {
				    		contactObj[col.fluroField.key] = [];
				    	}

				    	if(row[col.key] && row[col.key].length) {
				    		var items = []; // array of string to search or add as a tag

				    		// use key value as tag when column is yes or true
				    		if(isAMatch(row[col.key], 'yes') || isAMatch(row[col.key], 'true')) {
				    			items.push(col.key);
				    		} else if(isAMatch(row[col.key], 'no') || isAMatch(row[col.key], 'false')) {
				    			// do nothing
				    		}
				    		else // expect string 'realm1, realm2, some realm'
				    		{
				    			items = _
						    		.chain(row[col.key].split(splitRegex))
						    		.compact()
						    		.uniq()
						    		.value();
				    		}

				    		_.forEach(items, function(item) {
				    			// find matching realm
				    			// $log.info('case realms finding realms', service.config.availableRealms);
				    			var found = _.find(service.config.availableRealms, function(realm){
				    				return isAMatch(item, realm.title);
				    			});

				    			if(found){
				    				$log.debug('ImportContactsService.getMappedRow case realms found a matching realm', item, found);
				    				contactObj[col.fluroField.key].push(found); // add realm to contactObj
				    			} else {
				    				// $log.debug('ImportContactsService.getMappedRow case realms creating a realm', item);
				    				var newRealm = {
				    					title: item.trim(),
				    				};
				    				promises.push(FluroContent.resource('realm').save(newRealm).$promise
				    					.then(function(res){
				    						$log.debug('ImportContactsService.getMappedRow case realms NEW realm created', res);
				    						service.config.availableRealms.push(res); //add newly created realm as availableRealms;
				    						contactObj[col.fluroField.key].push(res); // add newly created realm to contactObj
				    					}));
				    			}
				    		});
				    	}
				    	break;
				    case 'tags':
				    	// make sure it starts with an array
				    	if(!angular.isDefined(contactObj[col.fluroField.key])) {
				    		contactObj[col.fluroField.key] = [];
				    	}

				    	if(row[col.key] && row[col.key].length) {

				    		var items = []; // array of string to search or add as a tag

				    		// use key value as tag when column is yes or true
				    		if(isAMatch(row[col.key], 'yes') || isAMatch(row[col.key], 'true')) {
				    			items.push(col.key);
				    		} else if(isAMatch(row[col.key], 'no') || isAMatch(row[col.key], 'false')) {
				    			// don't create this as a tag
				    		} else {

				    			// expect string 'tag1, tag2, some tag'
				    			items = _
						    		.chain(row[col.key].split(splitRegex))
						    		.compact()
						    		.uniq()
						    		.value();
				    		}

				    		_.forEach(items, function(item) {
				    			// find existing tag
				    			var found = _.find(service.config.availableTags, function(tag){
				    				return isAMatch(item, tag.title);
				    			});

				    			if(found){
				    				// $log.info('found a matching tag', item, found);
				    				contactObj[col.fluroField.key].push(found); // add tag to contactObj
				    			} else { //create a new tag
				    				// $log.info('creating a tag', item);
				    				var newTag = {
				    					title: item.trim(),
				    				};
				    				promises.push(FluroContent.resource('tag').save(newTag).$promise
				    					.then(function(res){
				    						// $log.info('NEW tag created', res);
				    						service.config.availableTags.push(res); //add newly created tag as availableTags;
				    						contactObj[col.fluroField.key].push(res); // add newly created tag to contactObj
				    					}))
				    			}
				    		});

				    	}
				    	break;
						case 'gender':
							var input;
							if(row[col.key]) {
								input = service.trimAndReplace(row[col.key]);
							}
							var output = "";

							switch (input) {
								case 'male':
								case 'm':
									output = "male";
									break;
								case 'female':
								case 'f':
									output = "female";
									break;
								default:
									output = "unknown";
									break;
							}
							contactObj[col.fluroField.key] = output;
							break;
						default:

					    // console.log('TEST', col.fluroField.key, col.key)
			        contactObj[col.fluroField.key] = row[col.key];
			        break;

		       3} // end contact optgroup switch
					break;
				case 'family':
					switch (col.fluroField.key) {
						case 'addressLine1':
						case 'addressLine2':
						case 'suburb':
						case 'state':
						case 'country':
							if(!familyObj.address) {
								familyObj.address = {};
							}
							familyObj.address[col.fluroField.key] = row[col.key];
							break;
						case 'postalCode':
							if(!familyObj.address) {
								familyObj.address = {};
							}
							familyObj.address[col.fluroField.key] = parseInt(row[col.key]);
							break;
						// family postalAddress
						case 'postal.addressLine1':
						case 'postal.addressLine2':
						case 'postal.suburb':
						case 'postal.state':
						case 'postal.country':
							if(!familyObj.postalAddress) {
								familyObj.postalAddress = {};
							}
							familyObj.postalAddress[col.fluroField.key.slice(7)] = row[col.key];
							break;
						case 'postal.postalCode':
							if(!familyObj.postalAddress) {
								familyObj.postalAddress = {};
							}
							familyObj.postalAddress[col.fluroField.key.slice(7)] = parseInt(row[col.key]);
							break;

						default:
							familyObj[col.fluroField.key] = row[col.key];
					} // end family optgroup switch
					break;
				case 'teams':
			    	if(row[col.key] && row[col.key].length) {
			    		var items = []; // array of string to search or add as a tag

			    		// use key value as team name when column is yes or true
			    		if(isAMatch(row[col.key], 'yes') || isAMatch(row[col.key], 'true')) {
			    			items.push(col.key);
			    		} else if(isAMatch(row[col.key], 'no') || isAMatch(row[col.key], 'false')) {
			    			// make sure do nothing
			    		}
			    		else // expect string 'team1, team2, some team'
			    		{
			    			items = _
					    		.chain(row[col.key].split(splitRegex))
					    		.compact()
					    		.uniq()
					    		.value();
			    		}

			    		_.forEach(items, function(item) {
			    			// find matching team
			    			var found = _.find(service.config.availableTeams, function(team){
			    				return isAMatch(item, team.title);
			    			});

			    			if(found){
			    				$log.debug('ImportContactsService.getMappedRow case teams found a matching team', item, found);
			    				teams.push(found);
			    			} else {
			    				var newTeam = {
			    					title: item.trim(),
			    					allowProvisional: true,
			    					realms: service.config.realms //use default realms
			    				};
			    				promises.push(FluroContent.resource('team').save(newTeam).$promise
			    					.then(function(res){
			    						$log.debug('ImportContactsService.getMappedRow case teams NEW team created', res);
			    						service.config.availableTeams.push(res); // add newly created realm as availableTeams;
			    						teams.push(res); // add newly created team
			    					}));
			    			}
			    		});
			    	}
					break;
				case 'posts':
					if(!angular.isDefined(posts[col.fluroField.postDefinition])){
						posts[col.fluroField.postDefinition] = {
							data: {}
						}; // ==> ie. posts.comment
					}
					if(row[col.key] && row[col.key].length) {
						// posts.comment.data.body = 'value'
						posts[col.fluroField.postDefinition].data[col.fluroField.key] = row[col.key];
					} else {
						if(_.isEmpty(posts[col.fluroField.postDefinition].data)){
							delete posts[col.fluroField.postDefinition];
						}
					}
					break;
			} // end optgroup switch
		}); // end columns loop

		if(familyObj.postalAddress){
		 	var samePostal = _.chain(familyObj.postalAddress)
				.values()
				.compact()
				.value()
				.length;
			familyObj.samePostal = Boolean(!samePostal);
		}

		var mappedRow = {
			contact: contactObj,
			family: familyObj,
			teams: teams,
			posts: posts
		};

		return $q.all(promises).then(function(){
			return mappedRow;
		}, function(err){
			$log.error(err);
			return mappedRow;  //  return mappedRow anyway in case tag or realm creation fail
		})
	}

	/**
     * start import
     *
     * @param      {csvResult}  (array of JS row object with key,value) JS collection object from a converted csv
     * @param      {columns}  csv header to fluro mapping configuration
     * @callback      {onSuccessContact(res)}  success callback with resObj passed
     * @callback      {onFailedContact(err)}  error callback with errObj passed
     * @return     {promiseObj}
     */
	function startImport(csvResult, columns, extraColumns, onSuccessContact, onFailedContact){
		var deferred = $q.defer();
		if(!service.config.realms) {
			$log.error('ImportContactsService.startImport: service.config missing required attributes');
			return;
		}

		service.cancelled = false;

		$log.info('ImportContactsService.startImport service.config', service.config);

		// Create temporary array of families to check for matches before checking the server
		var createdFamilies = [];

		async.eachSeries(csvResult, function(row, nextRow){

			if(service.cancelled) {
				nextRow('CANCELLED');
				service.cancelled = false;
				return;
			}
			// console.log('Each row', row);

			getMappedRow(row, columns, extraColumns)
				.then(function(mappedRow){


					var contactObj = mappedRow.contact;

					contactObj.family = mappedRow.family;
					contactObj.optOutEmail = service.config.optOutEmail;
		        	contactObj.optOutSMS = service.config.optOutSMS;

		        	contactObj.realms = _.compact(contactObj.realms);
		        	if(!contactObj.realms.length){
		        		contactObj.realms = service.config.realms; //apply default realm
		        	}

		        	$log.debug('ImportContactsService.startImport contact, mappedRow', contactObj, mappedRow);
		        	// nextRow();

		        	fluroContactService.create(contactObj, {familyList:createdFamilies})
		            	.then(function(contactRes){

		            		// create all posts
		            		_.forEach(mappedRow.posts, function(post, definition){
	            				post.title = definition;
	            				post.realms = contactRes.realms;
	            				post.parent = contactRes._id;
	            				FluroContent.resource(definition).save(post).$promise
	            					.then(function(postRes){
	            						// $log.debug('ImportContactsService.startImport success posts', postRes);
	            					}, function(postErr){
	            						$log.error('ImportContactsService.startImport error creating post', postErr);
	            					})
		            		})

		            		// handle team (if there are teams)
		            		if(mappedRow.teams && mappedRow.teams.length) {
		            			_.forEach(mappedRow.teams, function(team){
		            				fluroContactService.addAsProvisionalMember(contactRes, team)
		            					.then(function(teamRes){
		            						// $log.debug('ImportContactsService.startImport success team', teamRes);
		            					}, function(teamErr){
		            						$log.error('ImportContactsService.startImport error creating post', teamErr);
		            					})
		            			})
		            		}

		            		// handle family
		            		if(contactRes.family) {


		            			// This goes and gets the family from the server using the family id,
		            			// and then puts it into the createdfamiliies array. If the
		            			// family is just a family obvject then it just sticks the whole object in the array.
		            			if(typeof contactRes.family === 'string') {
		            				FluroContent.resource('family/' + contactRes.family).get().$promise
										.then(function(newFam){
											createdFamilies.push(newFam);
											onSuccessContact(contactRes);
		                					nextRow();
										}, function(err){
											$log.debug(err);
											onSuccessContact(contactRes);
											nextRow();
										});
		            			} else {
		            				createdFamilies.push(contactRes.family);
		            				onSuccessContact(contactRes);
		        					nextRow();
		            			}
		            		} else {
		            			// If no contactRes.family then keep going anyway
			            		console.log('No family could be created - continuing', contactRes)
			            		onSuccessContact(contactRes);
			            		nextRow();
		            		}



			            }, function(err){
			            	onFailedContact(err);
			            	nextRow();
			            });

				})

        }, function(err){
            if(err) {
                $log.error(err);
            }
           	deferred.resolve('completed');
        });
        return deferred.promise;
	}


});
