app.service('csvDataWashService', function () {

	var service = {
		csvToJs: csvToJs //convert csv string to js object
	};

	return service;

	////////////////////////////////////////////
	////////////////////////////////////////////
	////////////////////////////////////////////

	//https://gist.github.com/jonmaim/7b896cf5c8cfe932a3dd
  //modified so headers support double quotes as well marked _FIX
  function csvToJs(csv) {

    // var lines=csv.split("\n");
    var lines=csv.split(new RegExp('\n(?![^"]*"(?:(?:[^"]*"){2})*[^"]*$)'));
    // var lines=csv.split(new RegExp('\n(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)'));
    var result = [];
    // var headers = lines[0].split(",");

    ////////////////////////////
    ////////////////////////////
    ////////////////////////////

		var headers = [];

    if(lines[0].includes("\'") || lines[0].includes("\"")) {
      var row = lines[0],
        queryIdx = 0,
        startValueIdx = 0,
        idx = 0;

      while (idx < row.length) {
        /* if we meet a double quote we skip until the next one */
        var c = row[idx];

        if (c === '"') {
          do { c = row[++idx]; } while (c !== '"' && idx < row.length - 1);
        }

        if (c === ',' || /* handle end of line with no comma */ idx === row.length - 1) {
          /* we've got a value */
          var value = row.substr(startValueIdx, idx - startValueIdx).trim();

          /* skip first double quote */
          if (value[0] === '"') { value = value.substr(1); }
          /* skip last comma */
          if (value[value.length - 1] === ',') { value = value.substr(0, value.length - 1); }
          /* skip last double quote */
          if (value[value.length - 1] === '"') { value = value.substr(0, value.length - 1); }

          // console.log('queryIdx, startValueIdx, idx, value:', queryIdx, startValueIdx, idx, value);

          headers[queryIdx++] = value;
          startValueIdx = idx + 1;
        }

        ++idx;
      }

    } else {
      headers = lines[0].split(",");
    }

    ////////////////////////////
    ////////////////////////////
    ////////////////////////////

    for(var i=1; i<lines.length; i++) {
      var obj = {};

      var row = lines[i],
        queryIdx = 0,
        startValueIdx = 0,
        idx = 0;

      if (row.trim() === '') { continue; }

      while (idx < row.length) {
        /* if we meet a double quote we skip until the next one */
        var c = row[idx];

        if (c === '"') {
          do { c = row[++idx]; } while (c !== '"' && idx < row.length - 1);
        }

        if (c === ',' || /* handle end of line with no comma */ idx === row.length - 1) {
          /* we've got a value */
          if(c !== ',' && (idx+1) === row.length) {  // _FIX - make sure last character is included
            var value = row.substr(startValueIdx, row.length - startValueIdx).trim();
          } else {
            var value = row.substr(startValueIdx, idx - startValueIdx).trim();
          }

          /* skip first double quote */
          if (value[0] === '"') { value = value.substr(1); }
          /* skip last comma */
          if (value[value.length - 1] === ',') { value = value.substr(0, value.length - 1); }
          /* skip last double quote */
          if (value[value.length - 1] === '"') { value = value.substr(0, value.length - 1); }

          var key = headers[queryIdx++];
          obj[key] = value;
          startValueIdx = idx + 1;
        }

        ++idx;
      }

      result.push(obj);
    }

		////////////////////////////

    return {
			headers: headers,
			result: result
		};

		////////////////////////////

  };

})
