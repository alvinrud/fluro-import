angular.module('fluro').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('accordion-realm-selector/accordion-realm-selector.html',
    "<form name=form class=panel-realm-selector ng-class=\"{\n" +
    "        'panel-success': model.length,\n" +
    "        'panel-danger': (!model.length && form.$dirty)\n" +
    "    }\"><div class=panel-heading ng-click=\"isCollapsed = !isCollapsed\"><span ng-transclude>Click to view realms</span> <small class=text-muted>({{ model.length}}/{{ realms.length }})</small> <i class=\"pull-right lnr\" ng-class=\"{\n" +
    "            'lnr-chevron-down': isCollapsed, \n" +
    "            'lnr-chevron-right': !isCollapsed}\"></i></div><div class=list-group uib-collapse=!isCollapsed><div class=list-group-item ng-repeat=\"realm in realms | orderBy:'created'\" ng-class=\"{\n" +
    "                'selected': realm.selected\n" +
    "            }\" ng-style=\"{\n" +
    "                    'border-left':'3px solid '+realm.bgColor\n" +
    "            }\" ng-click=\"realm.selected = !realm.selected; form.$dirty = true\"><span>{{ realm.title }}</span><div class=\"material-switch pull-right\"><input type=checkbox id={{realm._id}} ng-model=realm.selected><label for={{realm._id}} class=label-success></label></div></div></div></form>"
  );


  $templateCache.put('accordion/accordion.html',
    "<div class=accordion ng-class={expanded:settings.expanded}><div class=accordion-title ng-click=\"settings.expanded = !settings.expanded\"><div class=container-fluid><div class=text-wrap><h3 class=title><i class=\"fa fa-angle-right pull-right\" ng-class=\"{'fa-rotate-90':settings.expanded}\"></i> <span ng-transclude=title></span></h3></div></div></div><div class=accordion-body><div ng-class=\"{'container':wide, 'container-fluid':!wide}\"><div ng-class=\"{'text-wrap':!wide}\" ng-transclude=body></div></div></div></div>"
  );


  $templateCache.put('admin-date-select/admin-date-select.html',
    "<div class=dateselect ng-class={open:settings.open}><div class=btn-group><a class=\"btn btn-default\" ng-class={active:settings.open} ng-click=\"settings.open = !settings.open\"><i class=\"fa fa-calendar\"></i> <span ng-bind-html=\"readable | trusted\"></span></a></div><dpiv class=popup><div class=datetime><div uib-datepicker class=datepicker datepicker-options=datePickerOptions ng-model=settings.dateModel></div></div></dpiv></div>"
  );


  $templateCache.put('admin-realm-select/admin-realm-select.html',
    "<div class=realm-select ng-class={expanded:settings.show}><div class=input-group ng-click=\"settings.show = !settings.show\"><div class=realm-select-btn><div ng-show=model.length><span class=realm-pill style=\"background-color: {{realm.bgColor}}; color: {{realm.color}}\" ng-repeat=\"realm in model track by realm._id\"><span>{{realm.title}}</span></span></div><em class=text-muted ng-show=!model.length>None selected</em></div><div class=input-group-addon><i class=\"fa fa-fw fa-angle-right\" ng-class=\"{'fa-rotate-90':settings.show}\"></i></div></div><div class=realm-select-list><div class=list-group><a class=\"list-group-item small\" ng-class={active:selected(realm)} ng-click=toggle(realm) ng-repeat=\"realm in realms | orderBy:'title' track by realm._id\"><i class=\"fa fa-circle inline-circle\" style=\"color: {{realm.bgColor}}\"></i> <i class=\"pull-right fa fa-check\" ng-show=selected(realm)></i> <span>{{realm.title}}</span></a></div></div></div>"
  );


  $templateCache.put('extended-field-render/extended-field-render.html',
    "<div class=\"extended-field-render form-group\"><label ng-if=\"field.type != 'group'\">{{field.title}}</label><div field-transclude></div></div>"
  );


  $templateCache.put('extended-field-render/field-types/multiple-value.html',
    "<div ng-switch=field.type><div class=content-select ng-switch-when=reference><div class=\"content-list list-group\"><div class=\"list-group-item clearfix\" ng-repeat=\"item in model[field.key]\"><a ui-sref=viewContent({id:item._id})><div class=pull-left><img ng-if=\"item._type == 'image'\" ng-src=\"{{$root.getThumbnailUrl(item._id)}}\"> <i ng-if=\"item._type != 'image'\" class=\"fa fa-{{item._type}}\"></i> <i ng-if=\"item.definition == 'song'\" class=\"fa fa-music\" style=padding-right:10px></i> <span>{{item.title}}</span></div></a><div class=\"actions pull-right btn-group\"><a class=\"btn btn-tiny btn-xs\" ng-if=\"item.assetType == 'upload'\" target=_blank ng-href={{$root.getDownloadUrl(item._id)}}><i class=\"fa fa-download\"></i></a> <a class=\"btn btn-tiny btn-xs\" ng-if=canEdit(item) ng-click=editInModal(item)><i class=\"fa fa-edit\"></i></a></div></div></div></div><div ng-switch-default><ul><li ng-repeat=\"value in model[field.key]\">{{value}}</li></ul></div></div>"
  );


  $templateCache.put('extended-field-render/field-types/value.html',
    "<div ng-switch=field.type><div class=content-select ng-switch-when=reference><div class=\"content-list list-group\"><div class=\"list-group-item clearfix\" ng-init=\"item = model[field.key]\"><a ui-sref=viewContent({id:item._id})><div class=pull-left><img ng-if=\"item._type == 'image'\" ng-src=\"{{$root.getThumbnailUrl(item._id)}}\"> <i ng-if=\"item._type != 'image'\" class=\"fa fa-{{item._type}}\"></i> <span>{{item.title}}</span></div></a><div class=\"actions pull-right btn-group\"><a class=\"btn btn-tiny btn-xs\" ng-if=\"item.assetType == 'upload'\" target=_blank ng-href={{$root.getDownloadUrl(item._id)}}><i class=\"fa fa-download\"></i></a></div></div></div></div><div ng-switch-when=date>{{model[field.key] | formatDate:'j M Y'}}</div><div ng-switch-when=image><img ng-src=\"{{$root.asset.imageUrl(item._id)}}\"></div><div ng-switch-default><div ng-bind-html=\"model[field.key] | trusted\"></div></div></div>"
  );


  $templateCache.put('fluro-interaction-form/button-select/fluro-button-select.html',
    "<div id={{options.id}} class=\"button-select {{to.definition.directive}}-buttons\" ng-model=model[options.key]><a ng-repeat=\"(key, option) in to.options\" ng-class={active:contains(option.value)} class=\"btn btn-default\" id=\"{{id + '_'+ $index}}\" ng-click=toggle(option.value)><span>{{option.name}}</span><i class=\"fa fa-check\"></i></a></div>"
  );


  $templateCache.put('fluro-interaction-form/custom.html',
    "<div ng-model=model[options.key] compile-html=to.definition.template></div>"
  );


  $templateCache.put('fluro-interaction-form/date-select/fluro-date-select.html',
    "<div ng-controller=FluroDateSelectController><div class=input-group><input class=form-control datepicker-popup={{format}} ng-model=model[options.key] is-open=opened min-date=to.minDate max-date=to.maxDate datepicker-options=dateOptions date-disabled=\"disabled(date, mode)\" ng-required=to.required close-text=\"Close\"> <span class=input-group-btn><button type=button class=\"btn btn-default\" ng-click=open($event)><i class=\"fa fa-calendar\"></i></button></span></div></div>"
  );


  $templateCache.put('fluro-interaction-form/dob-select/fluro-dob-select.html',
    "<div class=fluro-interaction-dob-select><dob-select ng-model=model[options.key] hide-age=to.params.hideAge hide-dates=to.params.hideDates></dob-select></div>"
  );


  $templateCache.put('fluro-interaction-form/embedded/fluro-embedded.html',
    "<div class=fluro-embedded-form><div class=form-multi-group ng-if=\"to.definition.maximum != 1\"><div class=\"panel panel-default\" ng-init=\"fields = copyFields(); dataFields = copyDataFields(); \" ng-repeat=\"entry in model[options.key] track by $index\"><div class=\"panel-heading clearfix\"><a ng-if=canRemove() class=\"btn btn-danger btn-sm pull-right\" ng-click=\"model[options.key].splice($index, 1)\"><span>Remove {{to.label}}</span><i class=\"fa fa-times\"></i></a><h5>{{to.label}} {{$index + 1}}</h5></div><div class=panel-body><formly-form model=entry fields=fields></formly-form><formly-form model=entry.data fields=dataFields></formly-form></div></div><a class=\"btn btn-primary btn-sm\" ng-if=canAdd() ng-click=addAnother()><span>Add <span ng-if=model[options.key].length>another</span> {{to.label}}</span><i class=\"fa fa-plus\"></i></a></div><div ng-if=\"to.definition.maximum == 1 && options.key\"><formly-form model=model[options.key] fields=options.data.fields></formly-form><formly-form model=model[options.key].data fields=options.data.dataFields></formly-form></div></div>"
  );


  $templateCache.put('fluro-interaction-form/field-errors.html',
    "<div class=field-errors ng-if=\"fc.$touched && fc.$invalid\"><div ng-show=fc.$error.required class=\"alert alert-danger\" role=alert><span class=\"fa fa-exclamation\" aria-hidden=true></span> <span class=sr-only>Error:</span> {{to.label}} is required.</div><div ng-show=fc.$error.validInput class=\"alert alert-danger\" role=alert><span class=\"fa fa-exclamation\" aria-hidden=true></span> <span class=sr-only>Error:</span> <span ng-if=!to.errorMessage.length>'{{fc.$viewValue}}' is not a valid value</span> <span ng-if=to.errorMessage.length>{{to.errorMessage}}</span></div><div ng-show=fc.$error.email class=\"alert alert-danger\" role=alert><span class=\"fa fa-exclamation\" aria-hidden=true></span> <span class=sr-only>Error:</span> <span>'{{fc.$viewValue}}' is not a valid email address</span></div></div>"
  );


  $templateCache.put('fluro-interaction-form/fluro-interaction-input.html',
    "<div class=\"fluro-input form-group\" scroll-active ng-class=\"{'fluro-valid':isValid(), 'fluro-dirty':isDirty, 'fluro-invalid':!isValid()}\"><label><i class=\"fa fa-check\" ng-if=isValid()></i><i class=\"fa fa-exclamation\" ng-if=!isValid()></i><span>{{field.title}}</span></label><div class=\"error-message help-block\"><span ng-if=field.errorMessage>{{field.errorMessage}}</span> <span ng-if=!field.errorMessage>Please provide valid input for this field</span></div><span class=help-block ng-if=\"field.description && field.type != 'boolean'\">{{field.description}}</span><div class=fluro-input-wrapper></div></div>"
  );


  $templateCache.put('fluro-interaction-form/fluro-terms.html',
    "<div class=terms-checkbox><div class=checkbox><label><input type=checkbox ng-model=\"model[options.key]\"> {{to.definition.params.storeData}}</label></div></div>"
  );


  $templateCache.put('fluro-interaction-form/fluro-web-form.html',
    "<div class=fluro-interaction-form><div ng-if=!correctPermissions class=form-permission-warning><div class=\"alert alert-warning small\"><i class=\"fa fa-warning\"></i> <span>You do not have permission to post {{model.plural}}</span></div></div><div ng-if=\"promisesResolved && correctPermissions\"><div ng-if=debugMode><div class=\"btn-group btn-group-justified\"><a ng-click=\"vm.state = 'ready'\" class=\"btn btn-default\">State to ready</a> <a ng-click=\"vm.state = 'complete'\" class=\"btn btn-default\">State to complete</a> <a ng-click=reset() class=\"btn btn-default\">Reset</a></div><hr></div><div ng-show=\"vm.state != 'complete'\"><form novalidate ng-submit=vm.onSubmit()><formly-form model=vm.model fields=vm.modelFields form=vm.modelForm options=vm.options><div ng-if=model.data.recaptcha><div recaptcha-render></div></div><div class=\"form-error-summary form-client-error alert alert-warning\" ng-if=\"vm.modelForm.$invalid && !vm.modelForm.$pristine\"><div class=form-error-summary-item ng-repeat=\"field in errorList\" ng-if=field.formControl.$invalid><i class=\"fa fa-exclamation\"></i> <span ng-if=field.templateOptions.definition.errorMessage.length>{{field.templateOptions.definition.errorMessage}}</span> <span ng-if=!field.templateOptions.definition.errorMessage.length>{{field.templateOptions.label}} has not been provided.</span></div></div><div ng-switch=vm.state><div ng-switch-when=sending><a class=\"btn btn-primary\" ng-disabled=true><span>Processing</span> <i class=\"fa fa-spinner fa-spin\"></i></a></div><div ng-switch-when=error><div class=\"form-error-summary form-server-error alert alert-danger\" ng-if=processErrorMessages.length><div class=form-error-summary-item ng-repeat=\"error in processErrorMessages track by $index\"><i class=\"fa fa-exclamation\"></i> <span>Error processing your submission: {{error}}</span></div></div><button type=submit class=\"btn btn-primary\" ng-disabled=!readyToSubmit><span>Try Again</span> <i class=\"fa fa-angle-right\"></i></button></div><div ng-switch-default><button type=submit class=\"btn btn-primary\" ng-disabled=!readyToSubmit><span>{{submitLabel}}</span> <i class=\"fa fa-angle-right\"></i></button></div></div></formly-form></form></div><div ng-show=\"vm.state == 'complete'\"><div compile-html=transcludedContent></div></div></div></div>"
  );


  $templateCache.put('fluro-interaction-form/nested/fluro-nested.html',
    "<div><div class=form-multi-group ng-if=\"to.definition.maximum != 1\"><div class=\"panel panel-default\" ng-init=\"fields = copyFields()\" ng-repeat=\"entry in model[options.key] track by $index\"><div class=\"panel-heading clearfix\"><a ng-if=canRemove() class=\"btn btn-danger btn-sm pull-right\" ng-click=\"model[options.key].splice($index, 1)\"><span>Remove {{to.label}}</span><i class=\"fa fa-times\"></i></a><h5>{{to.label}} {{$index + 1}}</h5></div><div class=panel-body><formly-form model=entry fields=fields></formly-form></div></div><a class=\"btn btn-primary btn-sm\" ng-if=canAdd() ng-click=addAnother()><span>Add <span ng-if=model[options.key].length>another</span> {{to.label}}</span><i class=\"fa fa-plus\"></i></a></div><div ng-if=\"to.definition.maximum == 1 && options.key\"><formly-form model=model[options.key] fields=options.data.fields></formly-form></div></div>"
  );


  $templateCache.put('fluro-interaction-form/order-select/fluro-order-select.html',
    "<div id={{options.id}} class=fluro-order-select><div ng-if=selection.values.length><p class=help-block>Drag to reorder your choices</p></div><div class=list-group as-sortable=dragControlListeners formly-skip-ng-model-attrs-manipulator ng-model=selection.values><div class=\"list-group-item clearfix\" as-sortable-item ng-repeat=\"item in selection.values\"><div class=pull-left as-sortable-item-handle><i class=\"fa fa-arrows order-select-handle\"></i> <span class=\"order-number text-muted\">{{$index+1}}</span> <span>{{item}}</span></div><div class=\"pull-right order-select-remove\" ng-click=deselect(item)><i class=\"fa fa-times\"></i></div></div></div><div ng-if=canAddMore()><p class=help-block>Choose by selecting options below</p><select class=form-control ng-model=selectBox.item ng-change=selectUpdate()><option ng-repeat=\"(key, option) in to.options | orderBy:'value'\" ng-if=!contains(option.value) value={{option.value}}>{{option.value}}</option></select></div></div>"
  );


  $templateCache.put('fluro-interaction-form/payment/payment-method.html',
    "<hr><div class=payment-method-select><div ng-if=!settings.showOptions><h3 class=clearfix>{{selected.method.title}} <em class=\"pull-right small\" ng-click=\"settings.showOptions = !settings.showOptions\">Other payment options <i class=\"fa fa-angle-right\"></i></em></h3></div><div ng-if=settings.showOptions><h3 class=clearfix>Select payment method <em ng-click=\"settings.showOptions = false\" class=\"pull-right small\">Back <i class=\"fa fa-angle-up\"></i></em></h3><div class=\"payment-method-list list-group\"><div class=\"payment-method-list-item list-group-item\" ng-class=\"{active:method == selected.method}\" ng-click=selectMethod(method) ng-repeat=\"method in methodOptions\"><h5 class=title>{{method.title}}</h5></div></div></div><div ng-if=!settings.showOptions><div ng-if=\"selected.method.key == 'card'\"><formly-form model=model fields=options.data.fields></formly-form></div><div ng-if=\"selected.method == method && selected.method.description.length\" ng-repeat=\"method in methodOptions\"><div compile-html=method.description></div></div></div></div><hr>"
  );


  $templateCache.put('fluro-interaction-form/payment/payment-summary.html',
    "<hr><div class=payment-summary><h3>Payment details</h3><div class=form-group><div ng-if=modifications.length class=payment-running-total><div class=\"row payment-base-row\"><div class=col-xs-6><strong>Base Price</strong></div><div class=\"col-xs-3 col-xs-offset-3\">{{paymentDetails.amount / 100 | currency}}</div></div><div class=\"row text-muted\" ng-repeat=\"mod in modifications\"><div class=col-xs-6><em>{{mod.title}}</em></div><div class=\"col-xs-3 text-right\"><em>{{mod.description}}</em></div><div class=col-xs-3><em class=text-muted>{{mod.total / 100 | currency}}</em></div></div><div class=\"row payment-total-row\"><div class=col-xs-6><h4>Total</h4></div><div class=\"col-xs-3 col-xs-offset-3\"><h4>{{calculatedTotal /100 |currency}} <span class=\"text-uppercase text-muted\">{{paymentDetails.currency}}</span></h4></div></div></div><div class=payment-amount ng-if=!modifications.length>{{calculatedTotal /100 |currency}} <span class=text-uppercase>({{paymentDetails.currency}})</span></div></div></div>"
  );


  $templateCache.put('fluro-interaction-form/search-select/fluro-search-select-item.html',
    "<a class=clearfix><i class=\"fa fa-{{match.model._type}}\"></i> <span ng-bind-html=\"match.label | trusted | typeaheadHighlight:query\"></span> <span ng-if=\"match.model._type == 'event' || match.model._type == 'plan'\" class=\"small text-muted\">// {{match.model.startDate | formatDate:'jS F Y - g:ia'}}</span></a>"
  );


  $templateCache.put('fluro-interaction-form/search-select/fluro-search-select-value.html',
    "<a class=clearfix><span ng-bind-html=\"match.label | trusted | typeaheadHighlight:query\"></span></a>"
  );


  $templateCache.put('fluro-interaction-form/search-select/fluro-search-select.html',
    "<div class=fluro-search-select><div ng-if=\"to.definition.type == 'reference'\"><div class=list-group ng-if=\"multiple && selection.values.length\"><div class=list-group-item ng-repeat=\"item in selection.values\"><i class=\"fa fa-times pull-right\" ng-click=deselect(item)></i> {{item.title}}</div></div><div class=list-group ng-if=\"!multiple && selection.value\"><div class=\"list-group-item clearfix\"><i class=\"fa fa-times pull-right\" ng-click=deselect(selection.value)></i> {{selection.value.title}}</div></div><div ng-if=canAddMore()><div class=input-group><input class=form-control formly-skip-ng-model-attrs-manipulator ng-model=proposed.value typeahead-template-url=fluro-interaction-form/search-select/fluro-search-select-item.html typeahead-on-select=select($item) placeholder=Search typeahead=\"item.title for item in retrieveReferenceOptions($viewValue)\" typeahead-loading=\"search.loading\"><div class=input-group-addon ng-if=!search.loading ng-click=\"search.terms = ''\"><i class=fa ng-class=\"{'fa-search':!search.terms.length, 'fa-times':search.terms.length}\"></i></div><div class=input-group-addon ng-if=search.loading><i class=\"fa fa-spin fa-spinner\"></i></div></div></div></div><div ng-if=\"to.definition.type != 'reference'\"><div class=list-group ng-if=\"multiple && selection.values.length\"><div class=list-group-item ng-repeat=\"value in selection.values\"><i class=\"fa fa-times pull-right\" ng-click=deselect(value)></i> {{getValueLabel(value)}}</div></div><div class=list-group ng-if=\"!multiple && selection.value\"><div class=\"list-group-item clearfix\"><i class=\"fa fa-times pull-right\" ng-click=deselect(selection.value)></i> {{getValueLabel(selection.value)}}</div></div><div ng-if=canAddMore()><div class=input-group><input class=form-control formly-skip-ng-model-attrs-manipulator ng-model=proposed.value typeahead-template-url=fluro-interaction-form/search-select/fluro-search-select-value.html typeahead-on-select=select($item.value) placeholder=Search typeahead=\"item.name for item in retrieveValueOptions($viewValue)\" typeahead-loading=\"search.loading\"><div class=input-group-addon ng-if=!search.loading ng-click=\"search.terms = ''\"><i class=fa ng-class=\"{'fa-search':!search.terms.length, 'fa-times':search.terms.length}\"></i></div><div class=input-group-addon ng-if=search.loading><i class=\"fa fa-spin fa-spinner\"></i></div></div></div></div></div>"
  );


  $templateCache.put('fluro-interaction-form/value/value.html',
    "<div class=fluro-interaction-value style=display:none><pre>{{model[options.key] | json}}</pre></div>"
  );


  $templateCache.put('routes/confirm/confirm.html',
    "<div ng-switch=status class=flex><div class=text-wrap><div ng-switch-default><div class=\"text-center wrapper-sm\"><h4>You're all set to import</h4></div><label>Realms <em class=text-muted>If no realm can be set automatically</em></label><realm-select ng-model=config.realms ng-type=contact ng-definition=contact></realm-select></div><div ng-switch-when=importing><div class=\"text-center wrapper-sm\"><h4 ng-show=!$scope.isComplete()><i class=\"fa fa-spinner fa-spin\"></i><span>Importing</span></h4></div><div class=text-wrap><uib-progress max=csv.result.length><uib-bar value=csv.progress.successCount type=success><span ng-if=\"csv.progress.successCount > 10\">{{csv.progress.successCount}}</span></uib-bar><uib-bar value=csv.progress.errorCount type=danger><span ng-if=\"csv.progress.errorCount > 5\">{{csv.progress.errorCount}}</span></uib-bar></uib-progress></div></div><div class=footer><div class=container-fluid><a class=\"btn btn-default\" ui-sref=importContacts.match>Back</a> <button class=\"btn btn-primary pull-right\" ng-click=importContacts() ng-disabled=!config.realms.length>Import {{::csv.result.length}} contacts</button> <a class=\"btn btn-primary pull-right\" ng-click=stop() ng-if=\"status == importing\">Stop Import</a></div></div></div></div>"
  );


  $templateCache.put('routes/home/view.html',
    "<div class=\"bg-white border-bottom\"><div class=wrapper><div class=container-fluid><div class=text-wrap><h3>What would you like to import?</h3><div class=list-group><a class=list-group-item ui-sref=importContacts>Contacts</a></div></div></div></div></div>"
  );


  $templateCache.put('routes/import-progress/importProgress.html',
    "<div class=modal-header><h5 class=title ng-show=!$ctrl.isComplete()><i class=\"fa fa-spinner fa-spin\"></i> <span>Importing</span></h5><h5 class=title ng-show=$ctrl.isComplete()><i class=\"fa fa-check\"></i> <span>Import complete</span></h5></div><div class=modal-body><uib-progress max=$ctrl.resolve.csv.result.length><uib-bar value=$ctrl.resolve.csv.progress.successCount type=success><span ng-hide=\"$ctrl.resolve.csv.progress.successCount < 5\">{{$ctrl.resolve.csv.progress.successCount}}</span></uib-bar><uib-bar value=$ctrl.resolve.csv.progress.errorCount type=danger><span ng-if=\"$ctrl.resolve.csv.progress.errorCount < 5\">{{$ctrl.resolve.csv.progress.errorCount}}</span></uib-bar></uib-progress><div ng-show=$ctrl.isComplete()><p class=\"small text-muted\"><i class=\"fa fa-spinner fa-spin\"></i><span style=\"margin-left: 5px\">Finishing up...</span></p><button class=\"btn btn-primary btn-sm\" type=button ng-click=$ctrl.ok()>Close and View Result Now</button></div></div>"
  );


  $templateCache.put('routes/imported/list.html',
    "<div class=\"bg-white border-bottom\"><div class=wrapper><div class=container-fluid><div class=text-wrap ng-if=pastImports.length><h4>Here's a list of your past imports</h4><div class=list-group><a class=list-group-item ng-repeat=\"item in pastImports\" ui-sref=importedView({importId:item._id})><h4 class=list-group-item-heading>{{::item.title}}</h4><p class=list-group-item-text>Imported by {{ item.author.name }} at {{item.created | formatDate:'g:ia l jS M Y'}}</p></a></div></div><div class=text-wrap ng-if=!pastImports.length><h4>You have no past imports</h4><a ui-sref=home class=\"btn btn-primary\">Back Home</a></div></div></div></div>"
  );


  $templateCache.put('routes/imported/view.html',
    "<div class=\"bg-white border-bottom\"><div class=wrapper-xs><div class=container-fluid><div class=text-wrap><h4>{{asset.title}}</h4><p>{{::contacts.length}}/{{::asset.data.total}} contacts</p><p>{{::families.length}} families created</p><div class=list-group><a class=list-group-item ng-repeat=\"contact in contacts\" ng-href=https://admin.fluro.io/contact/{{contact._id}} target=_blank>{{$index + 1}}. {{::contact.title}}</a></div></div></div></div></div>"
  );


  $templateCache.put('routes/match/match.html',
    "<div class=wrapper-sm><div class=container-fluid><div class=\"text-wrap text-center\"><h4>Match columns</h4><p>Match columns from your CSV to the corresponding field in Fluro. Once done, tap next below</p></div></div></div><div class=column-slider-outer><div class=column-slider-inner><div class=container-fluid><div id=col-{{$index}} class=column ng-repeat=\"col in columns track by $index\" ng-hide=\"!showSkipped && col.state =='skipped'\" ng-class=\"{'panel-default': col.state == 'unmatched',   'panel-success': col.state == 'matched',   'panel-primary': col.state == 'edit',   'panel-skipped': col.state == 'skipped' }\"><div class=panel-heading><span><em ng-if=\"col.state == 'edit'\">Editing</em><strong>{{::col.key}}</strong></span> <i class=fa ng-class=\"{'fa-check-circle': col.state == 'matched'}\"></i></div><div class=list-group><div class=list-group-item ng-repeat=\"row in csv.result | orderBy:notEmptyCount | limitTo:4\">{{::row[col.key]}}</div></div><div class=panel-body ng-switch=col.state><div ng-switch-when=edit><label for=field{{::$index}} style=\"font-size: 0.9em; text-transform: none\">{{::col.key}} <span ng-show=col.fluroField.key>> {{col.fluroField.title}}</span></label><br><select id=field{{::$index}} class=\"form-control input-sm\" ng-model=col.fluroField ng-change=save(col) ng-options=\"friendlyTitle(field) group by field.optgroupTitle disable when disableOption(field) for field in contactModel\"><option value=\"\">Select Fluro field</option></select><div class=actions><button class=\"btn btn-sm btn-primary\" ng-click=save(col) ng-disabled=!col.fluroField>Save</button> <button class=\"btn btn-sm btn-default\" ng-click=skip(col)>Skip</button></div></div><div ng-switch-when=skipped><strong class=brand-danger>&nbsp;</strong><p class=\"indicator text-muted small\">Skipped</p><div class=actions><button class=\"btn btn-sm btn-primary\" ng-click=edit(col)>Change</button></div></div><div ng-switch-when=matched><strong class=brand-success>Matched</strong><p class=\"indicator text-muted small\">to {{col.fluroField.title}}</p><div class=actions><button class=\"btn btn-sm btn-primary\" ng-click=edit(col)>Edit</button></div></div><div ng-switch-when=unmatched><strong>&nbsp;</strong><p class=\"indicator small\">Not matched</p><div class=actions><button class=\"btn btn-sm btn-primary\" ng-click=edit(col)>Choose</button> <button class=\"btn btn-sm btn-default\" ng-click=skip(col)>Skip</button></div></div><div ng-switch-default><strong>{{::col.key}}</strong><p class=\"indicator text-muted\">text field</p><div class=actions><button class=\"btn btn-sm btn-primary\" ng-click=edit(col)>Change</button> <button class=\"btn btn-sm btn-default\" ng-click=skip(col)>Skip</button></div></div></div></div></div></div></div><div class=wrapper-xs style=\"margin-bottom: 56px\"><div class=container-fluid><div class=\"text-wrap text-center\"><div><em class=\"small text-muted\" ng-if=unmatched.length>{{unmatched.length}} unmatched columns</em> <em class=\"small text-muted\" ng-if=!unmatched.length>All columns have been matched</em></div><a class=\"btn btn-default btn-sm\" ng-click=\"showSkipped = !showSkipped\"><span ng-if=showSkipped>Hide Skipped</span> <span ng-if=!showSkipped>Show Skipped</span></a></div></div></div><div class=footer><div class=container-fluid><a class=\"btn btn-default\" ui-sref=importContacts.select><span>Back</span></a> <button class=\"btn btn-primary pull-right\" ng-click=nextStep() ng-disabled=disableNext()><span>Next</span></button> <span ng-show=disableNext() class=\"match-helper small text-muted pull-right\"><i class=\"fa fa-warning\"></i><em style=\"margin-left: 5px\">You have {{editColumns.length}} unsaved column<span ng-if=\"editColumns.length != 1\">s</span></em></span></div></div>"
  );


  $templateCache.put('routes/match/missing-fields/missingFields.html',
    "<div class=modal-header><button type=button class=close ng-click=$ctrl.cancel()><span>&times;</span></button><h5 class=title>Some Required Fields are not assigned</h5></div><div class=modal-body><div ng-repeat=\"field in $ctrl.requiredFields track by $index\" ng-switch=field.key><label>{{field.title }}</label><div ng-switch-default class=\"panel panel-white\"><div class=\"panel-body matched\" ng-show=!field.isMissing><i class=fa ng-class=\"{'fa-exclamation': field.isMissing, 'fa-check': !field.isMissing}\"></i> <span style=\"margin-left: 10px\">{{field.title}} matched with</span> <em class=text-muted><span ng-if=field.columns.length ng-repeat=\"col in field.columns\">'{{col.key}}{{$last ? '' : ', '}}'</span> <span ng-if=!field.columns.length>'{{field.matchedTo}}'</span></em></div><div class=panel-body ng-show=field.isMissing><em class=small>Which field do you want to match to {{field.title}} in Fluro?</em><div class=form-group><div class=row><div class=col-xs-8><select class=\"form-control input-sm\" ng-model=field.colSelectionKey id={{::$index}}-select><option value=\"\">Select field</option><option ng-repeat=\"col in $ctrl.filteredColumns track by $index\" value={{col.key}}>{{col.key}}</option></select></div><div class=col-xs-4><a class=\"btn btn-sm btn-block btn-primary\" ng-click=$ctrl.apply(field) ng-disabled=!field.colSelectionKey>Apply</a></div></div></div></div></div></div></div><div class=modal-footer><button class=\"btn btn-default pull-left\" type=button ng-click=$ctrl.cancel()>Go back</button> <button class=\"btn btn-primary\" type=button ng-click=$ctrl.ok() ng-disabled=!$ctrl.service.isRequiredFieldsMapped($ctrl.columns)>Ready</button></div>"
  );


  $templateCache.put('routes/results/result.html',
    "<div class=wrapper-sm><div class=container-fluid><div class=text-center><h1 ng-if=\"csv.progress.successCount == csv.imported.length\"><i class=\"fa fa-smile-o\"></i></h1><h4>Import Completed!</h4><p ng-if=\"csv.progress.successCount == csv.imported.length\">All contacts imported successfully. You're all done.<br></p><p ng-if=\"csv.progress.successCount < csv.imported.length\"><strong>{{csv.progress.successCount}}</strong> of {{csv.imported.length}} contacts imported successfully. {{csv.progress.errorCount}} contacts could not be imported.</p></div></div><div class=wrapper><div class=container-fluid><div ng-if=csv.progress.errorCount><div class=table-responsive><table class=\"table table-bordered table-condensed table-striped\"><tr><th ng-repeat=\"header in headers(errors) track by $index\">{{ header }}</th></tr><tr ng-repeat=\"row in errors\"><td ng-repeat=\"col in row\">{{col}}</td></tr></table></div><p class=text-center><button class=\"btn btn-danger\" ng-csv=errors csv-header=headers(errors) filename=importerrors_{{csv.fluroId}}.csv ng-disabled=\"csv.result.length != csv.imported.length\">Download Errors</button></p></div></div></div><div class=footer><div class=container-fluid><a class=\"btn btn-default\" ui-sref=importContacts.select><span>Start Over</span></a> <a class=\"btn btn-primary pull-right\" href=https://admin.fluro.io/contact target=_blank><span>View Contacts in Admin</span> <i class=\"fa fa-angle-right\"></i></a></div></div></div>"
  );


  $templateCache.put('routes/select/select.html',
    "<div class=\"bg-white border-bottom flex flex-column\" style=\"display: flex\"><div class=\"drop-container text-center\" ui-view><div class=csv-button id=import><ng-csv-import class=csv-input content=csv.content header=csv.header header-visible=csv.headerVisible separator=csv.separator separator-visible=csv.separatorVisible encoding=csv.encoding encoding-visible=csv.encodingVisible upload-button-label=csv.uploadButtonLabel accept=csv.accept callback=csv.callback></ng-csv-import><div class=wrapper style=margin:auto><div class=wrapper-title><h1 class=h2>Fluro Import Tool</h1><h6><span class=thin>Uploading to&nbsp;</span>{{::$root.user.account.title }}</h6></div><em class=text-muted>Click to select or drop your CSV file here</em><br><a class=\"btn btn-primary\"><i class=\"fa fa-download\"></i> <span>Choose File</span></a></div></div></div><div class=select-page-footer><div class=container-fluid><a class=\"btn btn-default\" ng-href=\"{{$root.asset.downloadUrl('58db2baca45c157b37ca179a', {filename:'fluro_import_example', extension:'csv'})}}\"><span>Download CSV Template</span></a></div></div></div>"
  );

}]);
