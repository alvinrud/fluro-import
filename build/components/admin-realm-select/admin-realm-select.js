app.directive('realmSelect', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            type: '@ngType',
            definition: '@?ngDefinition',
        },
        templateUrl: 'admin-realm-select/admin-realm-select.html',
        controller: 'RealmSelectController',
    };
});

app.controller('RealmSelectController', function($scope, $rootScope, FluroContent, FluroAccess) {

    $scope.settings ={};

    ////////////////////////////////////////////////////

    //Create a model if none exists
    if (!$scope.model) {
        $scope.model = [];
    }

    ////////////////////////////////////////////////////

    if(!$scope.definition || !$scope.definition.length) {
        $scope.definition = $scope.type;
    }

    ////////////////////////////////////////////////////


    if($rootScope.user.accountType == 'administrator') {
        FluroContent.resource('realm').query().$promise.then(function(res) {
            $scope.realms = res;
        }, function(err) {
            console.log('Realm Select ERROR', err);
        });
    } else {

        //Now we get the realms from FluroAccess
        $scope.realms =  FluroAccess.retrieveSelectableRealms('create', $scope.definition, $scope.type);
    }

    //////////////////////////////////

    $scope.$watch('realms', function(availableRealms) {
        if (availableRealms && availableRealms.length == 1 && !$scope.model.length) {
            //console.log('Select first realm')
            $scope.model = [availableRealms[0]];
        }
    })
    

    //////////////////////////////////

    $scope.selected = function(realm) {

        var realmID = realm;
        if(realmID._id) {
            realmID = realmID._id;
        }

        return _.some($scope.model, function(r) {

            if(r._id) {
                r = r._id;
            }

            return r == realmID;
        });
    }


    $scope.toggle = function(realm) {

        var realmID = realm._id;
        if(realmID._id) {
            realmID = realmID._id;
        }

        //////////////////////////////////////

        var selected = $scope.selected(realm);

        if(selected) {
           $scope.model = _.reject($scope.model, function(rid) {
                if(rid._id) {
                    rid = rid._id;
                }
                return (rid == realmID);
            });
        } else {
            $scope.model.push(realm);
        }
    }
    
    // //////////////////////////////////


    // $scope.includes = function(realm) {
    //     return _.some($scope.model, function(i) {
    //         if (_.isObject(i)) {
    //             return i._id == realm._id;
    //         } else {
    //             return i == realm._id;
    //         }
    //     });
    // }

    // $scope.toggle = function(realm) {

    //     var matches = _.filter($scope.model, function(r) {
    //         var id = r;
    //         if (_.isObject(r)) {
    //             id = r._id;
    //         }

    //         return (id == realm._id);
    //     })

    //     ////////////////////////////////

    //     if (matches.length) {
    //         $scope.model = _.reject($scope.model, function(r) {
    //             var id = r;
    //             if (_.isObject(r)) {
    //                 id = r._id;
    //             }

    //             return (id == realm._id);
    //         });
    //     } else {
    //         $scope.model.push(realm);
    //     }

    // }



});