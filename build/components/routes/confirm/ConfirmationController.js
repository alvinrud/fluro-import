ConfirmationController.resolve = {
	tags: function(FluroContentRetrieval) {
        var queryDetails = {
            "_type": "tag"
        }
        return FluroContentRetrieval.query(queryDetails, null, null, {
            select: '_id title'
        }, null);
    },
	realms: function(FluroContentRetrieval) {
        var queryDetails = {
            "_type": "realm"
        }
        return FluroContentRetrieval.query(queryDetails, null, null, {
            select: '_id title bgColor',
            sort: 'created'
        }, null);
    },
    teams: function(FluroContentRetrieval) {
        var queryDetails = {
            "_type": "team",
            "allowProvisional": true
        }
        return FluroContentRetrieval.query(queryDetails, null, null, {
            select: '_id title allowProvisional'
        }, null);
    },
}

function ConfirmationController($scope, $log, ImportContactsService, $state, FluroContent, realms, tags, teams, $window) {

	$scope.stop = function () {
		ImportContactsService.settings.cancelled = true;
	}


	$scope.status = "confirm";

	// var service = ImportContactsService;

	ImportContactsService.config = $scope.config = {
		optOutEmail: false,
		optOutSMS: false,
		realms: [], // accordion-realm-selector
		availableRealms: realms,
		availableTags: tags,
		availableTeams: teams,
		// importId: $scope.csv.fluroId
	};
	$scope.isCollapsed = true;
	

	$scope.csv.imported = [];
	$scope.csv.progress = {
		successCount: 0,
		errorCount: 0
	}

	if(!$scope.columns) {
		$state.go('importContacts.select'); //get user to select .csv
	}

	$scope.filteredColumns = _.filter($scope.columns, function (col) {
		return col.fluroField.key;
	});

	// Used for putting skipped columns on the data object
	var extraColumns = _.filter($scope.columns, function (col) {
		return !col.fluroField.key;
	});


	$scope.importContacts = function() {
		$scope.status = "importing";

		uploadCsvAsAsset()
			.then(function(res){
	        	$log.debug('uploadCsvAsAsset res',res);
	            $scope.csv.fluroId = res._id;
	            ImportContactsService.config.importId = res._id;
	            startImport();
	        }, function(err){
	        	$log.error('uploadCsvAsAsset err', err);
				$scope.csv.fluroId = 'failed to create';
				ImportContactsService.config.importId = 'failed to create';
				startImport(); // still start import anyway
	        });
	}

	function startImport() {
		// openImportProgressModal(); //

		function onSuccessContact(res) {
			$log.info('Upload success', res);
			res.importState = 'success'; //Set this contact importState to success
			$scope.csv.imported.push(res); //Add to the imported pile
			$scope.csv.progress.successCount++; //Update the success count
		}

		function onFailedContact(err) {
			$log.error('Upload error', err);
			err.importState = 'error'; //Mark contact as Error
			$scope.csv.imported.push(err); //Still stick it in the imported pile
			$scope.csv.progress.errorCount++; //increase the error count
		}

		$scope.$onInit = function() {
		    $scope.isComplete = function() {
		        var completed = $scope.csv.imported.length == $scope.csv.result.length;

		        return completed;
		    }
	    };

	    $log.info('startImport', $scope.csv.result, $scope.filteredColumns, extraColumns);

		ImportContactsService.startImport($scope.csv.result, $scope.filteredColumns, extraColumns,
			onSuccessContact, 
			onFailedContact
		).then(function(){
			$log.info('import complete');
			$state.go('importContacts.result');
		});
	}

	function uploadCsvAsAsset(){
  		// create provided file as fluro asset
        // var csvContent = "data:text/csv;charset=utf-8;base64," + $window.btoa($scope.csv.content);
        // var csvContent = $window.btoa($scope.csv.content);
        var csvContent = $window.btoa(unescape(encodeURIComponent($scope.csv.content)));

        var request = {
            title       : "Uploaded csv",
            realms      : angular.copy(realms[0]),  // check resolve
            data 		: {
            				source: 'io.fluro.import',
            				total: $scope.csv.result.length,
            				columns: $scope.columns, // store matching config to asset
            				config: ImportContactsService.config // capture configuration
		                },
            file        : {
                            filename : 'uploaded.csv',
                            data     : csvContent
                        }
        }
        return FluroContent.resource('importedData').save(request).$promise
  	}


	////////////////////////////////////////
	////////////////////////////////////////
	////////////////////////////////////////


	// Progress Modal

	// function openImportProgressModal() {
	// 	var modalInstance = $uibModal.open({
	// 	  animation: true,
	// 	  size: 'lg',
	// 	  backdrop: 'static',
	// 	  component: 'importProgress', // ./modals/importProgress.html
	// 	  resolve: {
	// 	  	csv: function(){
	// 	  		return $scope.csv;
	// 	  	}
	// 	  }
	// 	});

	// 	modalInstance.result.then(function(status) {
	// 	  $log.info(status);
	// 	}, function () {
	// 	  $log.info('modal-component dismissed at: ' + new Date());
	// 	});
    //  	};

	// function onSuccessContact(res) {
	// 	$log.info('Upload success', res);
	// 	res.importState = 'success';
	// 	$scope.csv.imported.push(res);
	// 	$scope.csv.progress.successCount++;
	// }

	// function onFailedContact(err) {
	// 	$log.error('Upload error', err);
	// 	err.importState = 'error';
	// 	$scope.csv.imported.push(err);
	// 	$scope.csv.progress.errorCount++;
	// }

	////////////////////////////////////////

	


	
}