app.service('contactModelService', function ($q, FluroContent, $log) {

	var deferred = $q.defer();

	// list of available fields to include in contact model idenfied by keys
	var contactKeys = [
		'created',
		'firstName',
		'lastName',
		'dob',
		'gender',
		'maritalStatus',
		'emails',
		'phoneNumbers',
		'realms',
		'tags',
	];
	var contactPromise = FluroContent.endpoint('defined/models/contact').query().$promise;

	////////////////////////////////////////////
	////////////////////////////////////////////
	// Family
	// model https://apiv2.staging.fluro.io/defined/models/family
	// note it's not populating address fields, that's why it's hardcoded below
	var family = [
	  {
	    "title": "Address Line 1",
	    "type": "string",
	    "description": "Address Line 1",
	    "directive": "input",
	    "minimum": 0,
	    "maximum": 0,
	    "key": "addressLine1"
	  },
	  {
	    "title": "Address Line 2",
	    "type": "string",
	    "description": "Address Line 2",
	    "directive": "input",
	    "minimum": 0,
	    "maximum": 0,
	    "key": "addressLine2"
	  },
	  {
	    "title": "Suburb",
	    "type": "string",
	    "description": "Suburb",
	    "directive": "input",
	    "minimum": 0,
	    "maximum": 0,
	    "key": "suburb"
	  },
	  {
	    "title": "State",
	    "type": "string",
	    "description": "State",
	    "directive": "input",
	    "minimum": 0,
	    "maximum": 0,
	    "key": "state"
	  },
	  {
	    "title": "Country",
	    "type": "string",
	    "description": "Country",
	    "directive": "input",
	    "minimum": 0,
	    "maximum": 0,
	    "key": "country"
	  },
	  {
	    "title": "Postal Code",
	    "type": "string",
	    "description": "Postal Code",
	    "directive": "input",
	    "minimum": 0,
	    "maximum": 0,
	    "key": "postalCode"
	  },
	  // end of address
		////////////////////////
		// postal address
		{
	    "title": "Postal Address - Address Line 1",
	    "type": "string",
	    "description": "Postal Address - Address Line 1",
	    "directive": "input",
	    "minimum": 0,
	    "maximum": 0,
	    "key": "postal.addressLine1"
	  },
	  {
	    "title": "Postal Address - Address Line 2",
	    "type": "string",
	    "description": "Postal Address - Address Line 2",
	    "directive": "input",
	    "minimum": 0,
	    "maximum": 0,
	    "key": "postal.addressLine2"
	  },
	  {
	    "title": "Postal Address - Suburb",
	    "type": "string",
	    "description": "Postal Address - Suburb",
	    "directive": "input",
	    "minimum": 0,
	    "maximum": 0,
	    "key": "postal.suburb"
	  },
	  {
	    "title": "Postal Address - State",
	    "type": "string",
	    "description": "Postal Address - State",
	    "directive": "input",
	    "minimum": 0,
	    "maximum": 0,
	    "key": "postal.state"
	  },
	  {
	    "title": "Postal Address - Country",
	    "type": "string",
	    "description": "Postal Address - Country",
	    "directive": "input",
	    "minimum": 0,
	    "maximum": 0,
	    "key": "postal.country"
	  },
	  {
	    "title": "Postal Address - Postal Code",
	    "type": "string",
	    "description": "Postal Address - Postal Code",
	    "directive": "input",
	    "minimum": 0,
	    "maximum": 0,
	    "key": "postal.postalCode"
	  },
	  // end of address
	];
	_.forEach(family, function(field) {
		// assign optgroup
		field.optgroup = 'family';
		field.optgroupTitle = 'Family Addresses'
	});

	////////////////////////////////////////////
	////////////////////////////////////////////
	// Team

	// var teamKeys = [
	// 	'title'
	// ];
	// var teamPromise = FluroContent.endpoint('defined/models/team').query({
	// }).$promise;

	var teams = [
	  {
		"title": "Team Names",
		"type": "string",
		"description": "The title of this content",
		"directive": "input",
		"minimum": 1,
		"maximum": 1,
		"key": "title"
	  },
	 ];
	 _.forEach(teams, function(field) {
		// assign optgroup
		field.optgroup = 'teams';
		field.optgroupTitle = 'Groups/Teams';
	});

	////////////////////////////////////////////
	////////////////////////////////////////////
	// Posts

	var postsPromise = FluroContent.endpoint('defined/types/post').query({
		postParentTypes: "contact",
		searchInheritable: true
	}).$promise;

	////////////////////////////////////////////
	////////////////////////////////////////////

	$q.all([
		contactPromise,
		postsPromise,
		// teamPromise
	]).then(function(res){
		var model = _
				.chain(res[0])
				.filter(function(o) {
					var index = _.indexOf(contactKeys, o.key);
					if(index==-1) {
						return false;
					} else {
						return true;
					}
				})
				.forEach(function(field) {
					// assign optgroup
					field.optgroup = 'contact';
					field.optgroupTitle = 'Contact';
				})
				.value();

		// merge family into model
		model = model.concat(family);

		// handle team
		// var teamModel = _
		// 	.chain(res[2])
		// 	.filter(function(o) {
		// 		var index = _.indexOf(teamKeys, o.key);
		// 		if(index==-1) {
		// 			return false;
		// 		} else {
		// 			return true;
		// 		}
		// 	})
		// 	.forEach(function(field) {
		// 		// assign optgroup
		// 		field.optgroup = 'Groups/Teams';
		// 	})
		// 	.value();
		// model = model.concat(teamModel);
		model = model.concat(teams);

		// handle posts
		var postModels = _
			.chain(res[1])
			.filter(function(o){
				var parentTypes = _.get(o, 'data.postParentTypes', []);
				return !parentTypes.length || _.find(parentTypes, "contact");
			})
			.forEach(function(o){
				var includedFields = _.filter(o.fields, function(field){
					switch (field.type) {
						case 'string':
						case 'number':
							return true
							break;
						default:
							return false
							break;
					}
				});
				_.forEach(includedFields, function(field){
					field.optgroup = 'posts';
					field.optgroupTitle = 'Posts';
					field.title = o.title + " - " + field.title;
					field.postDefinition = o.definitionName;
				})
				model = model.concat(includedFields);
			})
			.value();

		$log.debug('contactModelService.resolve', model);

		deferred.resolve(model);
	})


	return deferred.promise;


})
