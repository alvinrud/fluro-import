ImportedViewController.resolve = {
    asset: function(FluroContent, $stateParams) {
        return FluroContent.resource('importedData/'+$stateParams.importId).get().$promise;
    },
    contacts: function(FluroContentRetrieval, $stateParams) {
        var queryDetails = {
            "_type": "contact"
        }
        return FluroContentRetrieval.query(queryDetails, null, null, {
            select: '_id title data',
        }, null).then(function(res){
            return _.filter(res, {data:{importId: $stateParams.importId}});
        });
    },
    families: function(FluroContentRetrieval, $stateParams) {
        var queryDetails = {
            "_type": "family"
        }
        return FluroContentRetrieval.query(queryDetails, null, null, {
            select: '_id title data',
        }, null).then(function(res){
            return _.filter(res, {data:{importId: $stateParams.importId}});
        });
    },
}

function ImportedViewController($scope, $log, asset, contacts, families) {
    $scope.asset = asset;
    $scope.contacts = contacts;
    $scope.families = families;
}