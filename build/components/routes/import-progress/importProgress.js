app.component('importProgress', {
  templateUrl: 'routes/importContacts/modals/importProgress.html',
  bindings: {
    resolve: '=',
    close: '&',
    dismiss: '&'
  },
  controller: function ($state, $timeout) {
    var $ctrl = this;

    $ctrl.$onInit = function () {

      $ctrl.isComplete = function(){
        var isComplete = $ctrl.resolve.csv.imported.length == $ctrl.resolve.csv.result.length;
        if(isComplete) {
          $timeout($ctrl.ok, 3000);
        }
        return $ctrl.resolve.csv.imported.length == $ctrl.resolve.csv.result.length;
      }
    };


    $ctrl.ok = function () {
      $ctrl.close({$value: 'ok'});
    };

    $ctrl.cancel = function () {
      $ctrl.dismiss({$value: 'cancel'});
    };

  }
});