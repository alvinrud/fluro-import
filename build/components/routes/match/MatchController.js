MatchController.resolve = {
    // assetRealm: function(FluroContentRetrieval) {
    //     var queryDetails = {
    //         "_type": "realm"
    //     }
    //     return FluroContentRetrieval.query(queryDetails, null, null, {
    //         select: '_id title',
    //         sort: 'created',
    //         limit: 1
    //     }, null);
    // },
}

function MatchController($scope, $log, $state, $uibModal, ImportContactsService, FluroContent) {

	$scope.showSkipped = false;
	var service = ImportContactsService;

	// console.log('STATE', $scope, service);

	// make sure csv $scope.columns is there
	if(!$scope.columns) {
		$state.go('importContacts.select'); //get user to select .csv
	}

	//////////////////////////////////////////////
	//////////////////////////////////////////////

	// $scope.$watch('$scope.columns', refreshColumns, true);
	function refreshColumns(){
		$scope.unmatched = _.filter($scope.columns, {state: 'unmatched'});
		$scope.editColumns = _.filter($scope.columns, {state: 'edit'});
	}
	refreshColumns();

	//////////////////////////////////////////////
	//////////////////////////////////////////////
	// alert: automatically matched
	//////////////////////////////////////////////

	$scope.autoMatched = _.filter($scope.columns, {state: 'matched'});
	if($scope.autoMatched.length){
		$scope.showAlert = true;
	}

	$scope.hideAlert = function(){
		$scope.showAlert = false;
		$scope.autoMatched = angular.copy($scope.autoMatched);
	}

	//////////////////////////////////////////////
	//////////////////////////////////////////////
	// view utilities
	//////////////////////////////////////////////
	
	$scope.save = function(col){
		col.state = 'matched';
		refreshColumns();
	}

	$scope.edit = function(col) {
		col.state = 'edit';
		refreshColumns();
	}

	$scope.skip = function(col) {
		col.fluroField = '';
		col.state = 'skipped';
		refreshColumns();
	}

	$scope.skipAll = function(){
		_.forEach($scope.unmatched, $scope.skip);
    }

    $scope.disableOption = function(field){
    	return !service.isFieldSelectable(field, $scope.columns);
    }

    // for when select a field option is disabled
    $scope.friendlyTitle = function(field){
    	var isDisabled = $scope.disableOption(field);

    	if(isDisabled){
    		var match = _.find($scope.columns, {fluroField:field});
    		return  field.title + ' (matched with ' + match.key + ')';
    	} else {
    		return field.title;
    	}
    }

	$scope.disableNext = function(){
		return _.some($scope.columns, function(o){
			return o.state == 'edit';
		});
	}

	// count how many empty value in {key:val} pair for ordering
	$scope.notEmptyCount = function(o) {
    	var count = 0;
    	_.forEach(o, function(val, key){
			if(_.isEmpty(val)){
				count += 1;
			}
		})
        return count;
    }

	//////////////////////////////////////////////
	//////////////////////////////////////////////
	// Next
	//////////////////////////////////////////////
	
	$scope.nextStep = function(){
		if(!service.isRequiredFieldsMapped($scope.columns)){
			openMissingFieldsModal(); //when some required fluro fields hasn't been matched
		} else {
			$scope.skipAll();
			_.forEach($scope.editColumns, function(col) {
				col.state = 'skipped';
			});
			$state.go('importContacts.confirm');
		}
	}

	function openMissingFieldsModal() {
		var modalInstance = $uibModal.open({
		  animation: true,
		  size: 'lg',
		  component: 'missingFields',
		  resolve: {
		    columns: function () {
		        return $scope.columns;
		    },
		    requiredFields: function() {
		    	return service.getRequiredFields(angular.copy($scope.columns));
		    },
		    saveCol: function() {
		    	return $scope.save;
		    }
		  }
		});

		modalInstance.result.then(function(status) {
		  $log.info(status);
		}, function () {
		  $log.info('modal-component dismissed at: ' + new Date());
		});
  	};


  	//////////////////////////////////////////////
	//////////////////////////////////////////////


}