ImportedListController.resolve = {
    pastImports: function(FluroContentRetrieval, DISTRIBUTION_KEY) {
        var queryDetails = {
            "_type": "asset",
            "definition": "importedData"
        }
        return FluroContentRetrieval.query(queryDetails, null, null, {
            select: '_id title created data author',
        }, null).then(function(res){
            return _.filter(res, {data:{source:DISTRIBUTION_KEY}});
        });
    }
}

function ImportedListController($scope, $log, pastImports) {
    $scope.pastImports = pastImports;
}