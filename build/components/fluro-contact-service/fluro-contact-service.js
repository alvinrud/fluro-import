app.service('fluroContactService', function($q, $log, $http, Fluro, FluroContent, FluroContentRetrieval, fluroFamilyService) {

    var service = {
        update      : update,
        create      : create,
        remove      : remove,
        invite      : invite,
        reinvite    : reinvite,
        addFamily   : addFamily,
        addDetail   : addDetail,
        addAsProvisionalMember : addAsProvisionalMember,
        getVcardUrl : getVcardUrl,
        getRecommendedFamily : getRecommendedFamily,
    };

    return service;

    //////////////////////////////////
    //////////////////////////////////
    //////////////////////////////////
    
    /**
     * invite existing contact as user
     *
     * @param      {contactObject}  contact  The contact
     * @return     {promiseObj}
     */
    function invite(contact) {

        // only take what's necessary from contact
        var userObj = {
            firstName       : contact.firstName,
            lastName        : contact.lastName,
            collectionEmail : contact.emails[0],
            permissionSets  : contact.permissionSets,
            realms          : contact.permissionSets[0].realms
        };
        
        return FluroContent.resource('persona').save(userObj).$promise;
    }
    
    //////////////////////////////////

    /**
     * reinvite already invited user
     *
     * @param      {string}  personaID  The persona id
     * @return     {promiseObj}
     */
    function reinvite(personaID) {
        return $http.post(Fluro.apiURL + '/user/reinvite/' + personaID).$promise;
    }

    //////////////////////////////////
    
    /**
     * create fluro contact and assign family based on lastName and contactObj.family.address
     *
     * @param      {contactObj}  contact  contact object to send
     * @param      {contactObj.family}  optional new family object to create
     * @param      {config.familyList}  optional array of preferred family to go through first before search server
     * @return     {promiseObj}
     */
    function create(contact, config) {
        $log.debug('fluroContactService.create contact', contact, config);

        var deferred = $q.defer();

        if(!config){ config = {}; }; // default config

        // turn single email string into array
        if(contact.emails && typeof contact.emails === 'string') {
            contact.emails = [contact.emails];
        }

        // turn single phone number string into array
        if(contact.phoneNumbers && typeof contact.phoneNumbers === 'string') {
            contact.phoneNumbers = [contact.phoneNumbers];
        }

        // if gender is empty
        if(!contact.gender) {
            contact.gender = 'unknown';
        }

        // if contact.family and lastname provided
        if(contact.family && contact.lastName && contact.lastName != '?') {
            
            if(contact.family.address) {
                config.populateAll = true;
            }

            // config.familyList is provided
            if(config.familyList && contact.family && contact.family.title) {
                // first find from provided list of family

                console.log('familylist ', config.familyList)
                var found = _
                    .find(config.familyList, function(item){
                        // check lastname first
                        if(item.title.trim().toUpperCase() === contact.family.title.trim().toUpperCase()) {
                            // return result if address match addresses
                            return fluroFamilyService.isMatchAddresses(item.address, contact.family.address);
                        } else {
                            return false;
                        }
                    });

                if(found) { // use matched family
                    contact.family = found; //add family match family
                    
                    // add emails, phone number to family
                    // fluroFamilyService.update(found, [contact]);
                    
                    $log.debug('fluroContactService config.familyList found and used', found, contact);
                    deferred.resolve(FluroContent.resource('contact').save(contact).$promise);
                } 
                else  // if not found search server
                {
                    var searchConfig = {
                        populateAll: false
                    };

                    // REUSABLE - create new family, assign newly created family, and create contact
                    function newFamilyAndCreateContact(){
                        fluroFamilyService.create(angular.copy(contact.family), [contact])
                            .then(function(res){
                                //add new family to contact
                                contact.family = res; 
                                FluroContent.resource('contact').save(contact).$promise
                                        .then(function(res){
                                            deferred.resolve(res);
                                        }, function(err){
                                            deferred.reject(err);
                                        })
                            }, function(err){
                                // when new family fails, create contact as it is without family
                                $log.error('newFamilyAndCreateContact failed to create a new family', err);
                                delete contact.family;
                                FluroContent.resource('contact').save(contact).$promise
                                        .then(function(res){
                                            deferred.resolve(res);
                                        }, function(err){
                                            deferred.reject(err);
                                        })
                            });
                    } // newFamilyAndCreateContact END
                    
                    // start searching server
                    getRecommendedFamily(contact.lastName, searchConfig)
                        .then(function(res){
                            $log.debug('getRecommendedFamily recommendation', res);
                            if(res.length) { 
                                //try to match them
                                var found = _
                                    .find(res, function(item){
                                        var isMatch;
                                        // check lastname first
                                        if(item.title.trim().toUpperCase() === contact.family.title.trim().toUpperCase()) {
                                            // check addresses
                                            isMatch = fluroFamilyService.isMatchAddresses(item.address, contact.family.address);
                                        } else {
                                            isMatch = false;
                                        }
                                        return isMatch;
                                    });

                                if(found) { // use matched family
                                    contact.family = found; //add family match family
                                    // add phone number to family
                                    fluroFamilyService.update(found, [contact]);
                                    $log.debug('getRecommendedFamily family found and used', found, contact);
                                    FluroContent.resource('contact').save(contact).$promise
                                        .then(function(res){
                                            deferred.resolve(res);
                                        }, function(err){
                                            deferred.reject(err);
                                        })
                                    
                                } else { // new family
                                    $log.debug('getRecommendedFamily adding new family', res, contact);
                                    newFamilyAndCreateContact();
                                }
                            } 
                            else // when there's no recommendation -> add new family
                            {
                                $log.debug('fluroContactService.create no family recommendation - adding a new family', contact);
                                newFamilyAndCreateContact();
                            }
                        }, function(err){  //getRecommendedFamily failed
                            $log.error("fluroContactService.create can't get recommendation, adding a new family", contact);
                            newFamilyAndCreateContact();
                        }); //getRecommendedFamily END
                        
                } // not found search server end
            } // config.familyList is provided END

            
        }
         else // family not provided - just create contact
        {
            delete contact.family;
            FluroContent.resource('contact').save(contact).$promise
                .then(function(res){
                    deferred.resolve(res);
                }, function(err){
                    deferred.reject(err);
                })
        }

        return deferred.promise;
    }

    //////////////////////////////////

    function update(contact) {

        // turn single email string into array
        if(contact.emails && typeof contact.emails === 'string') {
            contact.emails = [contact.emails];
        }

        // turn single phone number string into array
        if(contact.phoneNumbers && typeof contact.phoneNumbers === 'string') {
            contact.phoneNumbers = [contact.phoneNumbers];
        }

        // if gender is empty
        if(!contact.gender) {
            contact.gender = 'unknown';
        }

        return FluroContent.resource('contact/' + contact._id).update(contact).$promise;
    }

    //////////////////////////////////

    function remove(contact) {
        return FluroContent.resource('contact/' + contact._id).delete().$promise;
    }

    //////////////////////////////////

    function addFamily(contact, familyId) {
        contact.family = familyId;
        return FluroContent.resource('contact/' + contact._id).update(contact).$promise;
    }

    //////////////////////////////////

    function addDetail(contactDetail) {
        if(contactDetail._id) {
            return FluroContent.resource(contactDetail.definition + '/' + contactDetail._id).update(contactDetail).$promise;
        } else {
            return FluroContent.resource(contactDetail.definition).save(contactDetail).$promise;
        }
    }

    //////////////////////////////////

    /**
     * Add an existing contact or create a new contact as provisional member 
     *
     * @param      {contactObject}  contact  The contact
     * @param      {teamObject}  team object
     * @return     {promiseObj}
     */
    function addAsProvisionalMember(contact, team) {
        // if(team.provisionalMembers && contact._id) {
        //     if( _.find(team.provisionalMembers, {_id: contact._id}) ) {
        //         $log.debug('contact is already a member', team, contact);
        //         return $q.reject('contact is already a member');
        //     }
        // }
        return FluroContent.endpoint('teams/' + team._id + '/join').save(contact).$promise;
    }

    //////////////////////////////////

    function getVcardUrl(contact) {
        var contactId = (typeof contact === 'string') ? contact : contact._id;
        return Fluro.apiURL + '/contact/' + contactId + '/vcard.vcf';
    }

    //////////////////////////////////

    /**
     * Get recommended family based on keyword
     *
     * @param      {contactObject}  contact  The contact
     * @param      {config.populateAll}  boolean //populate all
     * @param      {config.populateAll}  boolean
     * @return     {promiseObj}
     */
    function getRecommendedFamily(keyword, config) {
        $log.debug('getRecommendedFamily', keyword, config);
        var deferred = $q.defer();

        // FluroContent.endpoint('content/family/search/' + keyword).query({
        //     limit: 3
        // }).$promise
        //     .then(function(res){
        //         if(res.length && config.populateAll){
        //             var ids = [];
        //             _.forEach(res, function(item) {
        //                 ids.push(item._id);
        //             });
        //             FluroContentRetrieval.retrieveMultiple(ids).then(function(res){
        //                 deferred.resolve(res);
        //             }, function(err){
        //                 deferred.reject(err);
        //             })
                    
        //         } else {
        //             return deferred.resolve(res);
        //         }
        //     }, function(err){
        //         deferred.reject(err);
        //     });
        deferred.resolve([]);

        return deferred.promise;
    }

    //////////////////////////////////
    
    // function hasSameAddress(address1, address2) {
        
    // }

    //////////////////////////////////

});